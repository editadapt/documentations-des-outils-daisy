Les outils de production du consortium Daisy
============================================

Resumé
------

Les formats Daisy et epub3 reposent sur un langage de balisage, c\'est à
dire un fichier texte où chaque élément est placé entre deux étiquettes
(tags en anglais) :

La norme utilisée pour assurer la cohérence du balisage est xml dtbook
pour les formats Daisy, et html pour epub3. Trois outils principaux
permettent de les manipuler. Ils sont disponibles sous licence LGPL, une
licence libre qui permet des extensions privées.

-   → OBI : logiciel de production de livres audios voix humaine
    uniquement. Facile d\'accès et d'utilisation.
-   → TOBI : logiciel de production multimédia, permet l'édition et
    l'import textes et audio.
-   → DAISY PIPELINE : ensemble d'outils et de scripts de transformation
    de fichiers.

### Daisy Pipeline : transformations et automatisation

Outil de transformation de fichiers dont les fonctionnalités peuvent
être étendues par des scripts reposant sur le langage xslt.

Permet la production automatisée en voix de synthèse, ou la
transformation de formats existants vers d'autre formats.

La version 2 apporte plus de fonctionnalités et une meilleure prise en
charge des formats epub3. L'interface est en anglais.

Une fois installé, les fonctionnalités sont accessibles en lignes de
commandes ou via une interface graphique. Il est également possible de
l'installer sur un serveur et d'y accéder via un navigateur web pour
faciliter les opérations de maintenance.

### Obi : outil de production audio structuré

Pensé pour être facile à utiliser avec peu de formation, il peut-être
utilisé avec peu de ressources. Il est destiné aux usagers individuels
et aux petites et moyennes structures de production.

Permet la production de fichiers audio voix humaine au format daisy
audio seulement. Il peut importer des fichiers mp3 préalablement
enregistrés.

Il inclut une version allégée du Daisy Pipeline.

### Tobi : outil de production multimédia

Il permet la production de livres audio texte inclus (connu comme Full
Daisy) qui assure une synchronisation entre l'affichage et la lecture
ainsi que des options de recherche et de manipulation du document
avancées (numéro des pages, table des contenus hiérarchisée, images,
appels de notes, etc.)

Il inclut un outil d'enregistrement avec des fonctions d'édition
basiques qui peuvent être complétés par des plugins pour répondre à des
besoins spécifiques. Il est également possible d'importer des fichiers
audio aux formats WAV et MP3. Il inclut une version allégée du Daisy
Pipeline.

Annexe technique
================

Daisy Pipeline 2 : scripts
--------------------------

<http://daisy.github.io/pipeline/Get-Help/User-Guide/Scripts/>

-   DAISY 2.02 Validator 
-   DAISY 2.02 to EPUB 3 
-   DAISY 3 to DAISY 2.02 
-   DAISY 3 to EPUB 3 
-   DTBook Validator 
-   DTBook to DAISY 3 
-   DTBook to EPUB3 
-   DTBook to HTML 
-   DTBook to ODT 
-   DTBook to PEF 
-   DTBook to RTF 
-   DTBook to ZedAI 
-   EPUB 3 Enhancer 
-   EPUB 3 Validator 
-   EPUB 3 to DAISY 2.02 
-   EPUB 3 to PEF 
-   HTML to EPUB3 
-   HTML to PEF 
-   NIMAS Fileset Validator 
-   ZedAI to EPUB 3 
-   ZedAI to HTML 
-   ZedAI to PEF 

Daisy Pipeline 2 : Nordic scripts
---------------------------------

<https://github.com/nlbdev/nordic-epub3-dtbook-migrator>

-   EPUB3 to DTBook
-   EPUB3 to HTML
-   EPUB3 Validator
-   EPUB3 ASCIIMath to MathML
-   HTML to EPUB3
-   HTML to DTBook
-   HTML Validator
-   DTBook to EPUB3
-   DTBook to HTML
-   DTBook Validator
