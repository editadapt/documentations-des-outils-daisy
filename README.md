# Documentation des outils Daisy
Version française de différentes documentations et guides de démarrage rapides pour utiliser les outils du consortium Daisy. 

→ **WordToEpub** : un convertisseur de fichiers docx vers epub3 incluant les principales fonctions d'accessibilité. https://daisy.org/activities/software/wordtoepub/

→ **OBI** : logiciel de production de livres audios voix humaine
uniquement. Facile d'accès et d'utilisation. https://daisy.org/activities/software/obi/

→ **TOBI** : logiciel de production multimédia, permet l'édition et
l'import textes et audio. https://daisy.org/activities/software/tobi/

→ **DAISY PIPELINE** : ensemble d'outils et de scripts de transformation
de fichiers. https://daisy.github.io/pipeline/
