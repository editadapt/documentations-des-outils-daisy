# Tobi : production de livres multimédias accessibles.


Auteur : Prashant Ranjan Verma, traduction et ajouts : Gautier Chomel

Tobi est un outils d'édition pensé pour les enrichissements de
publications. Par exemple ajouter une narration en voix de synthése à
parit d'un document texte seul ou réviser et améliorer un livre au
format Daisy. Il permet de créer ou d'éditer des livres aux formats
Daisy et Epub3. Tobi supporte aussi les contenus aux formats MathML et
SVG ainsi que les descriptions longues d'images.

L'interface est traduite en Français, mais pas la documentation, cette
derniére est disponible en anglais depuis le menu Aide, ainsi qu'à
l'adresse <http://www.daisy.org/tobi/tobi-user-reference-manual>

Ce guide explique une méthode de travail sans avoir à comprendre la
logique compléte du logiciel.

Ressources (Anglais)

**La page de Tobi :**
[**http://www.daisy.org/tobi/**](http://www.daisy.org/tobi/)

**La page de Save as Daisy :**
[**http://www.daisy.org/project/save-as-daisy**](http://www.daisy.org/project/save-as-daisy)

**Des vidéos pour apprendre à utiliser Tobi :**
[**http://www.youtube.com/playlist?list=PL2AD5CAAE9E6FABD1**](http://www.youtube.com/playlist?list=PL2AD5CAAE9E6FABD1)

**Le forum déidé à Tobi :**
[**http://www.daisy.org/forums/tobi**](http://www.daisy.org/forums/tobi)

## 1. Préparer le projet

Tobi peut importer des fichiers textes seulement au format xml DTBook.
Ce dernier peut-être créé en utilisantle plugin Save as Daisy (Microsoft
word 2013) ou Odt 2 Daisy (Libre Office) ou par conversion en utilisant
le Daisy Pipeline (voir les ressources proposées en introduction de ce
document).

S'assurerd'avoir marqué correctement tous les élèments de structure tels
que les sections, les numéros de pages, les notes de bas de pages,
abréviations et tables avec les styles appropriés dans l'éditeur de
texte avant la conversion vers xml DTBook.

1.  Ouvrez **Tobi**
2.  Utilisez **Ctrl+I** ou le menu **File / Import**, utlisez la fenétre
    de navigation pour sélectionner le fichier .xml que vous souhaitez
    utiliser et appuyez sur **Open**.
3.  Une boite de dialogue vous demande de confirmer les spécifications
    audios du projet. En cas d'importation d'un livre audio existant
    vous pouvez conserver les spécifications d'origine (**Preserve Audio
    Format**).

Tobi importe le document et affiche sa structure dans le **volet de
Navigation** (*Navigation panel* en haut à gauche), le contenu s'affiche
dans le **volet Contenu** (T*ext document view* en haut à droite).

Il est possible de vérifier les différents niveaux de structure (titres,
pages, marques et descriptions) en utilisant les onglets en bas du volet
de navigation. Il est possible de changer d'onglet en utilisant la
combinaison **Ctrl+Tab**.

## 2. Enregistrer la narration


Avec Tobi, la narration audio peut-être syncronisée avec le texte. Pour
cela il faut démarrer l'enregistrement et lire le texte surligné. Il
faut utiliser la combinaison Ctrl+R pour démarrer et arréter
l'enregistrement, et la combinaison **Ctrl+Shift+R** pour passer au
paragraphe suivant.

Ci-dessous une liste des commandes utiles pour les opérations
d'enregistrement :

-   Démarrer ou arréter l'egaliseur (monitoring) :\
    **Ctrl+M** ou menu **Audio / Enregistrement**
-   Démarrer ou arréter l'enregistrement :** Ctrl+R**
-   Enregistrer le pargaraphe suivante : **Ctrl+Shift+R**
-   Sélectionner le paragraphe précédent : **Ctrl+,**
-   Sélectionner le paragraphe suivant : **Ctrl+.**
-   Annuler l'enregistrement : **Esc**

Il est possible d'enregistrer dans la fenêtre principale ou dans le
Volet de Production (**Ctrl+N**), ainsi que de choisir d'afficher la
totalité du texte ou seulement le paragraphe en cours d'enregistrement
(**Ctrl+T**).

De nombreuses autres options pour la syncronisation du texte et de
l'audio sont disponibles, pour les connaîtres il faut se référer au
manuel de l'utilisateur (en anglais).

## 3. Éditer l'audio


L'enregistrement doit être révisé pour supprimer les sons non
volontaires, corriger les erreures de syncronisation et s'assurer de la
qualité de l'enregistrement. Il est possible d'utiliser la souris ou le
clavier pour naviguer dans l'enregistrement et éditer ce qui est
nécessaire. La liste suivante donne les principales fonctions et leur
raccourcis clavier.

- Aller au début de l'enregistrement : touche **Début** (ou Home).
-   Afficher tout l'audio : **Ctrl+W**
-   Zoomer sur l'audio sélectionné : **Ctrl+Shift+W**
-   Aller à la fin de l'enregistrement : Touche **Fin** (ou End).
-   Se déplacer au paragarphe précédent **Ctrl+Alt+Fléche Gauche**
-   Se déplacer au paragarphe suivant** Ctrl+Alt+Fléche Droite**
-   Retour en arriére d'une durée prédéterminée : **Ctrl+Fléche Gauche**
-   Avancer d'une durée prédéterminée : **Ctrl+Fléche Droite**
-   Sélectiopnner toute la piste audio **Ctrl+A**
-   Déselectionner : **Ctrl+D**
-   Sélectionner tout le contenu avant le curseur : **Shift+Début** (ou
    Home)
-   Sélectionner tout le contenu aprés le curseur : **Shift+Fin** (ou End)
-   Sélectionner l'audio du paragraphe suivant : **Shift+Ctrl+Alt+Fléche**
    Droite
-   Sélectionner l'audio du paragraphe précédent : **Shift+Ctrl+Alt+Fléche**
    Gauche
-   Commencer la sélection au curseur : **Ctrl+\**[ (Ctrl+Shift+Alt+5) ou
    clic
-   Arréter la sélection au curseur : **Ctrl+\] **(Ctrl+Shift+Alt+°) ou clic
-   Copier la portion audio sélectionnée : **Ctrl+C**
-   Couper la portion audio sélectionnée : **Ctrl+X**
-   Coller la portion audio sélectionnée :** Ctrl+V**
-   Supprimer la portion audio sélectionnée : **Suppr** (ou **Delete**)
-   Importer un fichier audio du disque dur : **Ctrl+I**
-   Générer une narration en voix de synthése pour le fragement
    sélectionné : **Ctrl+G**
-   Placer la suite de la portion audio dans le fragment de texte
    suivant : **Ctrl+Entré**

Dans Tobi, l'édition de l'audio est faite de façon non destructive,
c'est à dire que lorsque l'on supprime une portion, cette derniére est
toujours existante. Cette approche permet d'éditer l'audio rapidement
sans temps de chargement. À la fin des opérations d'édition il est
nécessaire d'opérer un nettoyage en utilisant le menu Edition /
Nettoyage (complet ou rapide) ou la combinaison de touches
**Ctrl+ShiftAlt+D**. Un dossier Delete est alors affiché par Tobi et peut
être mis à la corbeille pour gagner de l'espace de stockage si
nécessaire.

## 4. Métadonnées


La touche **F11** ouvre l'éditeur de métadonnées. Les touches **Fléche Haut** et
**Fléche Bas** permettent de sélectionner les différentes métadonnées et la
touche Tabulation (**Tab**) sélectionne le champ pour l'éditer. Il est
possible d'ajouter les métadonnées que l'on souhaite en utilisant le
bouton **Ajouter un nouvel élément**.

Plus un fichier contient de métadonnées précises, plus il
sera facile de l'identifier pour l'utilisateur comme pour le producteur.

Les métadonnées sont basées sur deux normes internationales : DublinCore
(dc) qui permet d'identifier des objets numériques, notamment les pages
internet, et Digital Talking Book (dtb) spécifiques au format Daisy.

Les métadonnées obligatoires sont :

-   **dc:Creator :** le nom du créateur du fichier.
-   **dc:Identifier** : un identifiant unique généré par le logiciel qui
    peut-être remplacé par un isbn si le livre numérique s'est vu
    assigné un isbn différent de celui de l'ouvrage original.
-   **dc:Language** : la langue de la publication au format RFC4646. Par
    exemple :

    -   fr : français (moderne)
    -   fr-FR : français de France, hors langues régionales normalisées
        (oc, ca, br, etc.)
    -   fr-BE : français de Belgique.

Liste complète à l'adresse :
 ([*https://docs.sdl.com/LiveContent/content/en-US/SDL\_MediaManager\_241/concept\_A9F20DF9433C46FF8FED8FA11A29FAA0*](https://docs.sdl.com/LiveContent/content/en-US/SDL_MediaManager_241/concept_A9F20DF9433C46FF8FED8FA11A29FAA0)).

-   **Dc:Title** : le titre de l'ouvrage.

Plusieurs autres ? sont importantes comme :

-   **dc:Source** permet de renseigner l'isbn de l'édition à partir de
    laquelle le livre a été adapté.
-   **dc:Right** permet de préciser le cadre légal d'exploitation du
    fichier. Par exemple la loi exception handicap.
-   **Dtb:Narrator** identifie le donneur de voix.

**Dc:Publisher** identifie le producteur du livre audio.

## 5. Exporter au format DAISY


Il est nécesdsaire de valider le projet avant son exportation. La touche
**F12** ouvre la boite de dialogue *Validation* qui contient plusieurs
onglets pour les différents types d'erreures (Métadonnées, balises ou
audio). Toutes les erreures doivent être corrigées pour finaliser le
livre.

Tobi exporte au format Daisy 3. Il est possible d'utiliser le Daisy
Pipeline pour convertir ces fichiers au format Daisy 2.02 pour les
besoins des lecteurs dont le matériel ne supporte pas Daisy 3.

Pour exporter le projet Tobi, Il faut appuyer sur **Ctrl+E** ou aller au
menu **Fichier / Exporter**. Une boite de dialogue permet de modifier les
paramétres de l'encodage audio, puis une seconde de choisir le dossier
pour enregistrer le livre exporté. Si aucun dossier n'est sélectionné,
Tobi exportera le livre dans un sous dossier portant la mention Export
du dossier de travail. À la fin de l'export, une nouvelle boite de
dialogue vous propose de vérifier le livre exporté (Daisy Check) pour
contrôler les erreures qui pourraient apparaitre à la conversion.

Il est important de conserver le dossier de travail du projet Tobi en
plus du dossier exporté en cas de nécessité d'édition futur.
