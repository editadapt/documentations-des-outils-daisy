Ce document est traduit de l'anglais, la version originale est disponible à cette [adresse](https://github.com/daisy/pipeline/blob/72df6d51690ead5374ac5df2e696e3fe2bca9842/webui/README.md)
 
# DAISY Pipeline 2 : Guide de l'utilisateur
Le pipeline est un outil informatique qui permet de générer convertir, valider et générer des fichiers audios voix de synthèse, braille et epub. 
Pour ce faire, il repose sur la gestion de scripts, la liste des scripts disponibles est à la fin de ce document. 
 
Il existe une version accessible en ligne de commandes (cmd.exe), une destinée à être accessible via un navigateur web, et une dotée d'une interface graphique en partie accessible.
 
Ce document concerne cette dernière version (Graphical User Interface ou GUI). 
 
## Démarrage rapide
Au lancement de Daisy Pipeline, une fenêtre apparaît : 

![Pipeline window](https://raw.githubusercontent.com/daisy/pipeline-gui/master/docs/img/window-init.png)
 
Pour commencer : **`File->New job`**, ou la commande clavier **`Cmd+N`**

![File menu](https://raw.githubusercontent.com/daisy/pipeline-gui/master/docs/img/menu-file.png)
 
Une fenêtre de création de tâche apparaît. Sélectionner un script dans le menu déroulant.

![Scripts menu](https://raw.githubusercontent.com/daisy/pipeline-gui/master/docs/img/menu-scripts.png)
 
Remplir le formulaire (qui diffère selon le script sélectionné).

![New job form](https://raw.githubusercontent.com/daisy/pipeline-gui/master/docs/img/new-job-daisy202-epub3.png)
 
Pour démarrer la tâche, utiliser le bouton  **`Run`** ou la commande clavier **`Cmd+R`**.
La tâche apparaît dans la barre latérale gauche de l'interface qui affiche son statut. Si une tâche est sélectionnée, la partie inférieur de la fenêtre affichera les messages relatifs à la conversion.

![Job running](https://raw.githubusercontent.com/daisy/pipeline-gui/master/docs/img/job-running.png)
 
Pendant qu'une tâche est en cours, il est possible d'en lancer une autre en utilisant le menu **`File / New job`**, ou la commande clavier **`Cmd+N`**
Lorsqu'un job est fini, son status apparaît comme `Done`. Pour consulter les détails du résultat, sélectionner la tâche.
 
![Job done](https://raw.githubusercontent.com/daisy/pipeline-gui/master/docs/img/job-done.png)
 
Les fichiers créés dans le dossier sélectionné lors de la création de la tâche. Si le ou les fichiers n'apparaissent pas, le fichier log ou le volet `Messages` références les erreures. 
Certaines peuvent dépendre de la description de la tâche, de la qualité du fichier, ou d'une erreur du système.
 
## Fonctions
 
**New job**
 
Raccourci clavier : `Cmd+N`
 
Créé une nouvelle tâche en sélectionnant un script et en renseignant les instructions particulières (emplacement des fichiers, langage du document, etc.). 
Le formulaire diffère selon le script sélectionné.
 
**Run job**
 
Raccourci clavier : `Cmd+R`
 
Lorsque le formulaire est renseigné, cette commande permet de démarrer la tâche. Elle apparaît dans le volet `Jobs` de la fenêtre. 
Si elle ne semble pas démarrer, consulter le détail des erreures dans le volet `Messages`.
 
 
**Delete job**
 
Raccourci clavier : `Delete`
 
Supprime une tâche de la liste. Ne supprime pas les fichiers créés.
 
**Run job again**
 
Raccourci clavier : `Cmd+Shift+R`
 
Permet de démarrer une nouvelle tâche à partir d'une en cours d'exécution ou terminée conservant ainsi ses paramètres
 
**Copy messages to clipboard**
Copie le contenu de la fenêtre `Messages`. Il est ensuite possible de les Coller dans un éditeur de texte ou un mail pour demander un éclaircissement ou rapporter des erreures de l'application.
 
**Check Updates**
 
Permet de mettre à jour le logiciel.
 
**User guide**
 
Renvoie à la version originale de ce document.
 
 
## Scripts inclus :
 
* [DAISY 2.02 Validator](http://daisy.github.io/pipeline/modules/daisy202-validator)
* [DAISY 2.02 to EPUB 3](http://daisy.github.io/pipeline/modules/daisy202-to-epub3)
* [DAISY 3 to DAISY 2.02](http://daisy.github.io/pipeline/modules/daisy3-to-daisy202)
* [DAISY 3 to EPUB 3](http://daisy.github.io/pipeline/modules/daisy3-to-epub3)
* [DTBook Validator](http://daisy.github.io/pipeline/modules/dtbook-validator)
* [DTBook to DAISY 3](http://daisy.github.io/pipeline/modules/dtbook-to-daisy3)
* [DTBook to EPUB3](http://daisy.github.io/pipeline/modules/dtbook-to-epub3)
* [DTBook to HTML](http://daisy.github.io/pipeline/modules/dtbook-to-html)
* [DTBook to PEF](http://daisy.github.io/pipeline/modules/braille/dtbook-to-pef)
* [DTBook to ZedAI](http://daisy.github.io/pipeline/modules/dtbook-to-zedai)
* [EPUB 3 Validator](http://daisy.github.io/pipeline/modules/epub3-validator)
* [EPUB 3 to DAISY 2.02](http://daisy.github.io/pipeline/modules/epub3-to-daisy202)
* [EPUB 3 to PEF](http://daisy.github.io/pipeline/modules/braille/epub3-to-pef)
* [HTML to EPUB3](http://daisy.github.io/pipeline/modules/html-to-epub3)
* [HTML to PEF](http://daisy.github.io/pipeline/modules/braille/html-to-pef)
* [NIMAS Fileset Validator](http://daisy.github.io/pipeline/modules/nimas-fileset-validator)
* [ZedAI to EPUB 3](http://daisy.github.io/pipeline/modules/zedai-to-epub3)
* [ZedAI to HTML](http://daisy.github.io/pipeline/modules/zedai-to-html)
* [ZedAI to PEF](http://daisy.github.io/pipeline/modules/braille/zedai-to-pef)
 
## Daisy Pipeline 2 : Nordic scripts
Cet ensemble de scripts peut être ajouté. 
https://github.com/nlbdev/nordic-epub3-dtbook-migrator
* EPUB3 to DTBook
* EPUB3 to HTML
* EPUB3 Validator
* EPUB3 ASCIIMath to MathML
* HTML to EPUB3
* HTML to DTBook
* HTML Validator
* DTBook to EPUB3
* DTBook to HTML
* DTBook Validator
 
## Ressources
 
*  [Documentation en anglais](Daisy Pipeline 2 GUI démarage rapide.md)
*  [Daisypedia](http://www.daisy.org/daisypedia/categories/publish)
*  [forums](http://www.daisy.org/forums/pipeline-2)  (en anglais).
 
