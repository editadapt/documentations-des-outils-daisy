<head>
<title>Split and Merge EPUB Tobi Projects</title>
<style>
h1 {font-size:16pt;}
h2 {font-size:14pt;}
h3 {font-size:13pt;}
h4 {font-size:12pt;}
p {font-size:12pt;}
</style>
</head>

<body>
<h1>Split and Merge EPUB Tobi Projects</h1>

<p>Tobi presents a flexible Split and Merge command for EPUB publications. It takes advantage of the modular structure of EPUB Tobi project and gives you the flexibility to subdivide only the large project spine items.</p>

<p>This document explains how to split an EPUB Tobi project. For splitting DAISY Tobi projects, please refer to <a href="split-and-merge-daisy-tobi-project.htm">Split and Merge DAISY Tobi Project</a>.</p>

<p>There are two methods of dividing the work for parallel production of EPUB Tobi projects: 
<br><a href="#method1">Method 1</a>: Use the existing modularity of EPUB Tobi project, and divide the work without using the Split and Merge command. 
<br><a href="#method2">Method 2</a>: Split the sub-projects that are large and require further segregation by using the Split and Merge command.</p>

<p><a name="method1"></a><h2>Method 1: Divide the work without using Split and Merge command</h2>
An EPUB Tobi project is a collection of sub-projects connected via project spine, which are stored separately as stand-alone Tobi projects. If the sub-projects are small and do not require further division, you can take the advantage of the modularity of the file structure of EPUB Tobi projects and achieve parallel production as follows.
<ol><li>Copy the XUK file and the data folder corresponding to a project spine item on to a different workstation.</li>
<li>In the copied Tobi project, add audio using live recording, importing audio files, or generating audio using synthetic voice.</li>
<li>Place the pair of XUK file and the data folder back replacing the original ones.</li>
</ol></p>

<p>However, if some of the items of project spine are large and require further split, follow the process described in Method 2.</p>

<p><a name="method2"></a></p><h2>Method 2: Divide the work using the Split and Merge command</h2>
This method uses the Split and Merge command to split EPUB Tobi projects. The Split and Merge command gives you the flexibility to split only those items of the project spine that are large and require further segregation. Other items, which will not be split, can be distributed among production workstations in their original form.</p>

<p>The process of splitting and merging the sub-projects can be divided in the following steps: 
<br /><a href="#step1">Step 1</a>: Split the project spine items that are large and require further segregation.
<br /><a href="#step2">Step 2</a>: Distribute the sub-projects among various workstations for parallel production. 
<br /><a href="#step3">Step 3</a>: Replace the sub-projects with the updated ones, and merge the split sub-projects.
<br /><a href="#step4">Step 4</a>: Execute the Merge command for project spine to create a new project spine with the updated and merged items.</p>

<p><a name="step1"></a><h3>Step 1: Split the large items of the project spine</h3>
As an EPUB Tobi project is a collection of sub-projects connected via project spine, the split function will work on each of them individually. It gives you a flexibility to split only those projects that are large in size and require further division. The Project Spine dialog displays the size of project file next to each spine item, enabling you to determine which projects should be divided.</p>

<p>This section lists the steps for splitting one sub-project (item of project spine). The rest will follow the similar lines.</p>

<p>Tobi splits the project on the position of document marks. So it is important to place document marks carefully. There can be some places where the audio corresponding to the child node is placed at its parent node. In such cases, you should place mark at the parent node only.</p>

<p>Document marks can be placed both in Navigation pane and Document pane, but it is recommended to do it in the Navigation pane as it gives the overall picture of the document. Follow the steps listed below to split an item of project spine using the Navigation pane.
<ol><li>Open the project spine by pressing <strong>Ctrl+O</strong> and selecting the file with .xukspine extension.</li>
<li>In the Project Spine dialog, select the project that should be split and click <strong>OK</strong>. Please note that the size of the project file for each spine item is displayed next to it. The selected project will be opened in the work area of Tobi.</li>
<li>Remove all the existing marks by clicking <strong>Text-&gt;Document marks-&gt;Remove all document marks</strong> on the menu or pressing <strong>Shift+Ctrl+K</strong>.</li>
<li>Go to the Navigation pane (Headings tab) by pressing <strong>F8</strong>.</li>
<li>Navigate to the item where the split-point should be marked, and press <strong>Enter</strong>. Alternatively, you can double-click on the respective item. If you are placing marks using the Document pane, there is no need to double-click or press Enter; you can place the marks directly.</li>
<li>Press <strong>Ctrl+K</strong> to mark out the split-point. You can navigate to the sub sections and mark the split-points at any level.</li>
<li>After marking out all the split-points, go to the Marks tab to verify them by pressing <strong>Ctrl+Tab</strong>.</li>
<li>Click <strong>Tools-&gt;Split/Merge projects-&gt;Split...</strong> on the menu or press <strong>Shift+Ctrl+B</strong>.</li>
</ol></p>

<p>Tobi creates a copy of the original sub-project (item of project spine) called the Master project. Then it splits the project at the marked split-points as follows:
<ul><li>The contents prior to the first split-point will be placed in the first split sub-project.</li>
<li>The contents between the first split-point and the second split-point will be placed in the second split sub-project.</li>
<li>And so on...</li></ul></p>

<p>Each split sub-project will carry a number suffix denoting its position in the original project.</p>

<p>After the split, Tobi will open the Master project. You will find that the document pane appears in varying colors. This is because Tobi marks out the split-point positions in red, and the contents of the split sub-project in pink. Please do not make any changes to the Master project, or the merge functionality may not work properly.</p>

<p>Now you can split another project spine item in the similar way.</p>

<p><h4>Directories</h4>
For each split project, Tobi creates a directory with �_SPLIT� prefix to store the Master and sub-projects for the respective project spine item. The Split directory will be stored in the main project directory itself, and there will be a separate directory for each split spine item.</p>

<p>The Master project will carry the same name as that of the original project, and it will be suffixed by the text "Master". Its project file will be named �master.xuk�.</p>

<p>The split sub-project directory will be suffixed with number denoting its position in the Master project. Each split sub-project directory will contain the data folder as well as the XUK file making it a standalone Tobi project.</p>

<p><a name="step2"></a><h3>Step 2: Distribute the work among various workstations for parellel production</h3>
When distributing the sub-projects, we should consider the fact that some of the project spine items have been split by the user and others have not. The split project spine items will have a directory with �_SPLIT� prefix in the same folder, and therefore it is easy to distinguish them from others.</p>

<p>Distribute the sub-projects among production workstations in the following way:
<ul><li>For each split project spine item, copy the sub-directories from the Split directory corresponding to that item.</li>
<li>For the project spine items that were not split, copy the corresponding .xuk file and the data folder.</li>
</ul></p>

<p>Now you can add audio content to the projects concurrently by live recording, importing audio files, or generating audio using synthetic voice.</p>

<p><a name="step3"></a>
<h3>Step 3: Replace the sub-projects with the updated ones, and merge the split-sub projects.</h3>
Collect the updated sub-projects and place them back in the following way.</p>

<p>The project spine items that were not split should be copied directly in the main project directory replacing the original items.</p>

<p>The project spine items that were split should be copied in their corresponding Split directory (with �_SPLIT� prefix), and then they should be merged back. The following instructions illustrate how to do it.
<ol><li>Place all the split sub-projects corresponding to a project spine item in the respective Split folder.</li>
<li>Open the Master project �master.xuk�. The directory with the "Master" suffix under the Split directory contains the Master project.</li>
<li>Click <strong>Tools-&gt;Split/Merge projects-&gt;Merge...</strong> on the menu or press <strong>Shift+Ctrl+Alt+B</strong>.</li>
<li>After merging, Tobi will ask to delete the directories and files created for the Split-merge process permanently. Click <strong><em>Yes</em></strong> to delete or <strong><em>No</em></strong> to ignore.</li>
<li>Tobi will again ask for confirmation as this operation cannot be undone. Press <strong><em>Yes</em></strong> again if you wish to delete the files.</li>
</ol></p>

<p>All the split sub-projects will be imported back in their respective positions, and the merged project will be opened in the work area of Tobi. Please note that Tobi does not make any changes to the Master project. It will create its copy and make the changes therein.</p>

<p>In the similar way, open the Master project of each split project spine item and follow the above steps till all the split projects are merged back.</p>

<p><h4>Directories</h4>
The merged project for each project spine item will be created in a new directory having �_MERGE� prefix under the main project directory. This project will be a complete Tobi project and will contain all the updated contents for the respective project spine item.</p>

<p><a name="step4"></a>
<h3>Step 4: Merge the project spine</h3>
When all the individual project spine items have been merged, we should merge the project spine of the EPUB book.</p>

<p>Open the project spine by any of the following methods:
<ul><li>If a sub-project is open in the work area of Tobi, press <strong>Ctrl+U</strong>.</li>
<li>Otherwise, press <strong>Ctrl+O</strong> and select the file with .xukspine extension.</li>
</ul></p>

<p>Before going further, check that all the split spine items have been merged. The merged spine items have maroon font-color in the Project Spine dialog. The checkbox <strong>Show merged spine items</strong> enables this functionality. If you uncheck this checkbox, Tobi displays the original spine items instead of the updated and merged ones.</p>

<p>Once you are sure that all the projects are updated and merged, click the Merge Full EPUB button.</p>

<p>A new project will be created with all the updated and merged spine items. The Merge command automatically picks the merged sub-projects from the Merge folder having �_MERGE� prefix. At the same time, it picks the project spine items that had not been split from the main project directory itself.</p>

<p>After merging all the project spine items, it stores the completely merged EPUB Tobi project in the directory named �_MERGE�, and opens it in the work area of Tobi.</p>

<p><h4>Directories</h4>
The finally merged project will be stored in the �_MERGE� directory under the main project directory. It will contain the contents of the EPUB publication in entirety.</p>

<p><h3>Recap of the directory structure</h3>
For the Split and Merge command, Tobi creates the following directories in the main project directory under the �_XUK� directory.
<ul><li>The Split and Merge feature does not alter the original project files. It creates a directory with �_SPLIT� prefix for each project spine item and performs all the operations there. The following types of items can be found in the Split directory.
<ol><li>Master project directory with "Master" suffix.</li>
<li>Number suffixed directories containing a split sub-project each.</li>
</ol></li>
<li>The merged project for each split project spine item is stored in a directory with �_MERGE� prefix.</li>
<li>The finally merged complete EPUB Tobi project can be found in the directory named �_MERGE�.</li>
</ul></p>

<p><a href="tobi-user-reference-manual.htm">Back to Manual Home</a></p>
</body>
