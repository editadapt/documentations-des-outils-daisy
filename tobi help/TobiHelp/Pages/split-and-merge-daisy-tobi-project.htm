<head>
<title>Split and Merge DAISY Tobi Projects</title>
<style>
h1 {font-size:16pt;}
h2 {font-size:14pt;}
h3 {font-size:13pt;}
h4 {font-size:12pt;}
p {font-size:12pt;}
</style>
</head>

<body>
<h1>Split and Merge DAISY Tobi Projects</h1>

<p>Tobi enables you to split a full-text full-audio DAISY book into several sub-projects and reassemble them back after updating.</p>

<p>This document explains how to split a DAISY Tobi project. For splitting EPUB Tobi projects, please refer to <a href="split-and-merge-epub-tobi-project.htm">Split and Merge EPUB Tobi Project</a>.</p>


<p>The process flow can be enumerated as follows: <br/>
<a href="#step1">Step 1.</a>Split a project into various sub-projects and a Master project. <br/>
<a href="#step2">Step 2.</a>Distribute the sub-projects among various team members for parallel production. <br/>
<a href="#step3">Step 3.</a>After recording and editing sub-projects, collect them and replace the application-generated sub-projects with the updated ones. <br/>
<a href="#step4">Step 4.</a>Merge them back in the Master project.</p>

<p>Now we will go through each of these steps in detail, and we will also look at the directories created during these operations.</p>

<p><h2><a name="step1"></a>Step 1: Split the project</h2>
Tobi splits the project on the position of document marks. So it is important to place document marks carefully. Especially in a full-text, full-audio book where you should not place a document mark at a child node if its audio is placed in the parent node.</p>

<p>You can place document marks in both Navigation pane and Document pane, but it is recommended to do it in the Navigation pane as it gives the overall picture of the document. Follow the steps listed below to split the project into sub-projects using the Navigation pane.</p>

<ol>
<li>Open the Tobi project by pressing <strong>Ctrl+O</strong> and selecting it from the dialog thus opened. If the Tobi project for the book does not exist, you should first <a href="tobi-project-and-dtb-creation.htm#importing_document_or_existing_book">import</a> it by pressing <strong>Ctrl+I</strong>.</li>
<li>Remove all the existing marks by clicking <strong>Text-&gt;Document marks-&gt;Remove all document marks</strong> on the menu or pressing <strong>Shift+Ctrl+K</strong>.</li>
<li>Go to the Navigation pane (Headings tab) by pressing <strong>F8</strong>.</li>
<li>Navigate to the item where the split-point should be marked, and press <strong>Enter</strong>. Alternatively, you can double-click on the respective item. If you are placing marks using the Document pane, there is no need to double-click or press Enter; you can place the marks directly.</li>
<li>Press <strong>Ctrl+K</strong> to mark out the split-point. You can navigate to the sub sections and mark the split-points at any level.</li>
<li>After marking out all the split-points, go to the Marks tab to verify them by pressing <strong>Ctrl+Tab</strong>.</li>
<li>Click <strong>Tools-&gt;Split/Merge projects-&gt;Split...</strong> on the menu or press <strong>Shift+Ctrl+B</strong>.</li>
</ol>

<p>Tobi creates a copy of the original project called the Master project. Then it splits the project at the marked split-points as follows:
<ul><li>The contents prior to the first split-point will be placed in the first sub-project.</li>
<li>The contents between the first split-point and the second split-point will be placed in the second sub-project.</li>
<li>And so on...</li>
</ul>
Each sub-project will carry a number suffix denoting its position in the main project.</p>

<p>After the split, Tobi will open the Master project. You will find that the document pane appears in varying colors. This is because Tobi marks out the split-point positions in red, and the contents of the sub-project in pink.</p>

<p><img src="UserManualImages/MasterProject.JPG" alt="Figure exemplifying the Master project"/></p>

<p>Please do not make any changes to the Master project, or the merge functionality may not work properly.</p>

<p><h3>Directories</h3>
A new directory will be created in your project directory named �_SPLIT�. This directory will contain the Master project directory as well as all the sub-project directories.</p>

<p>The Master project will carry the same name as that of the original project, and it will be suffixed by the text "Master". Its project file will be named �master.xuk�.</p>

<p>The sub-project directory will be suffixed with number denoting its position in the Master project; for example, if the original project was named �DAISY_Revolution�, the directory of its first sub-project will be named �DAISY_Revolution_0�, the second sub-project as "DAISY_Revolution_1", and so on. Each sub-directory will contain the data folder as well as the XUK file making it a standalone Tobi project.</p>

<p><h2><a name="step2"></a>Step 2: Distribute the work</h2>
Now that the project is divided into several sub-projects, which are complete Tobi projects, you can distribute them among the team members by simply copying the respective directories on to their systems.</p>

<p><h2><a name="step3"></a>Step 3: Collect the sub-projects and replace the earlier ones</h2>
Once the sub-projects have been updated, collect them and place them back in the �_SPLIT� directory replacing the application-generated sub-projects.</p>

<p><h2><a name="step4"></a>Step 4: Merge the sub-projects in to Master</h2>
Finally, we should merge the updated sub-projects back in to the Master project. Follow the steps listed below to accomplish it.</p>
<ol>
<li>Open the Master project �master.xuk�. Please note that the directory with the "Master" suffix contains the Master project.</li>
<li>Click <strong>Tools-&gt;Split/Merge projects-&gt;Merge...</strong> on the menu or press <strong>Shift+Ctrl+Alt+B</strong> to merge the project.</li>
<li>After merging the projects, Tobi will ask to delete the sub-projects and files created for the Split-merge process permanently. Click <strong>Yes</strong> to delete or <strong>No</strong> to ignore.</li>
<li>Tobi will again ask for confirmation as this operation cannot be undone. Press <strong>Yes</strong> again if you wish to delete the files.</li>
</ol>

<p>Tobi will import all the sub-projects back in their respective positions, and then it will open the merged project in its work area. Please note that Tobi does not make any changes to the Master project; it will create its copy and make the changes therein.</p>

<h3>Directories</h3>
<p>The merged project will be created in a new directory, called �_MERGE�, under the project directory. This project will be a complete Tobi project and will contain all the updated contents.</p>

<h2>Recap of the directory structure</h2>
<ul>
<li>The project merge feature does not alter the original project files. It creates a sub-directory of the project directory called "_SPLIT" and performs all the operations there. The following type of items can be found under the "_SPLIT" directory:</li>
<ol>
<li>Master project directory with "Master" suffix.</li>
<li>Number suffixed directories containing a sub-project each.</li>
</ol>
<li>The finally merged project can be found in the "_MERGE" directory under the project directory.</li>
</ul>

<p><a href="tobi-user-reference-manual.htm">Back to Manual Home</a></p>

</body>
