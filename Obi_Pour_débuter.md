

Débuter avec Obi
================

*Auteur original : Prashant Ranjan Verma, traduction et adaptation :
Gautier Chomel.*

Le projet OBI est hébergé à l'adresse
[http://www.daisy.org/obi.](http://www.daisy.org/obi)On y trouve les
versions du logiciel à télécharger, la documentation et les informations
sur le développement. Vous pouvez aussi consulter le
[Forum](http://www.daisy.org/forums/obi) la
[FAQ](http://www.daisy.org/obi/faq) et regarder les vidéos [Obi
Training](http://www.daisy.org/daisypedia/obi-training-videos).
L'ensemble de ces ressources est en anglais.

L'interface du logiciel est disponible en français, ainsi qu'une
documentation accessible en appuyant sur la touche **F1** ou depuis le
menu **Aide / Contenu**. Une version au format html de cette
documentation est disponible en ligne (voir Liens utiles).

Des ressources en Français pourront être disponibles sur le
site[http://daisy-france.org](http://daisy-france.org/).

Ce guide du débutant propose une démarche simple pour créer un livre
audio aux formats Daisy 2.02 ou 3.Ce n'est pas un manuel complet, mais
il aide à créer votre premier livre sans acquérir une compréhension
complète du logiciel.

L'interface Française doit être sélectionnée via le menu **Tools /
Préférences**, onglet **User Profile** et menu déroulant **langage**. Le
logiciel doit être redémarré pour que le changement soit effectif.

Interface
---------

L'interface d'OBI est composée de 6 éléments accessibles par des
raccourcis claviers :
-   Volet de la table des contenus (TOC) en haut à gauche (**F6**)
-   Volet des conte­nus à droite (**F6**)
-   Volet navigation en bas (**Ctrl+F6**)
-   Volet des métadonnées en bas à droite (**Ctrl+Alt+M**)
-   Séparateur de panneaux

Il est aussi possible de naviguer entre ces vues en utilisant les
combinaisons de touches **Ctrl+Tab** ou **Ctrl+Shift+Tab**.

Liens utiles :
--------------

En Français :

-   Le site du groupe de travail Daisy France :http://daisy-france.org
-   Aide complète à télécharger :\
    [https://gitlab.com/editadapt/documentations-des-outils-daisy/](https://gitlab.com/editadapt/documentations-des-outils-daisy/blob/master/Obi_Aide.html)

En anglais :

-   Obi Project Home
-   Obi Forum
-   Obi Frequently Asked Questions (FAQs).
-   Get started with Obi
-   Obi: Various methods for authoring content
-   Obi Training Videos
-   Description of various functions in Obi

 
-

Pas à pas pour créer un livre
-----------------------------

Le processus de création d'un livre audio se résume en 6 phases :

-   Structuration préalable (table des matières)
-   Enregistrement ou importation de l'audio
-   Amélioration du niveau de structuration : pagination, notes de bas
    de page... (optionnel)
-   Entrée des métadonnées
-   Finalisation
-   Exportation au format DAISY et validation

### 1. Créer un nouveau projet

Démarrez Obi.

Une boîte de dialogue apparaît. Choisissez Créer un nouveau projet,

une nouvelle boîte de dialogue apparaît. Donnez un **titre** à votre
projet, généralement le titre du livre. Laisser cocher la case **Créer
automatiquement un titre de section** ainsi que le champ **ID**, à moins
que l'organisation dispose d'un dispositif de numérotation
d'identification unique spécifique (comme un isbn). Il est également
possible de **choisir l'emplacement des fichiers** du projet.

Une boîte de dialogue vous demande de spécifier les paramètres audio du
projet.

### 2. Créer la structure

Obi affiche désormais un nouveau projet. La structure se limite au titre
du projet. Il est nécessaire de reproduire la structure du livre sous
forme de sections (par exemple couverture, préambule, contenu et
quatrième de couverture) et de sous sections (par exemple chapitre 1,
chapitre 2, colophon, annexes, résumé, etc.)

Les éléments de structure se créent dans le Volet *TOC* (à gauche). Les
principales fonctions et leurs raccourcis claviers sont les suivantes :

-   Ajouter une section (**Ctrl+H**)
-   Ajouter une sous-section (**Ctrl+Shift+H**)
-   Insérer une section (**Ctrl+Alt+H**)
-   Seul le contenu de la section sélectionnée est visible dans le Volet
    *Contenu.*  

### 3. Placer le contenu

Les fichiers audio doivent être placés dans chaque élément de structure
auquel ils correspondent, soit par enregistrement, soit par import de
fichiers existants.

#### **Pour enregistrer :**
-   S'assurer que le micro est branché et ouvrir **l'égaliseur**
    (**Alt+Ctrl+P**)
-   Sélectionner une section dans le Volet *table des matières* ou
    sélectionner le Volet contenu. Utiliser **Ctrl+R** ou cliquer sur le
    bouton rouge de la barre de navigation pour démarrer le monitoring
    pré enregistrement. Le niveau d'enregistrement du micro apparaît
    dans l'égaliseur graphique et dans l'égaliseur textuel. Il est
    possible d'ajuster le volume du microphone, peut être ajusté en
    utilisant l'outil de contrôle du son de système d'exploitation.
-   Utiliser à nouveau **Ctrl+R** ou cliquer sur le bouton rouge du
    volet de navigation pour commencer l'enregistrement.
-   Pour démarrer l'enregistrement sans monitoring, Utilisez
    **Ctrl+Shift+R**.
-   Durant l'enregistrement il est possible de :
-   Marquer le début et la fin d'une section
-   Marquer le début et la fin d'une page
-   Marquer le début ou la fin d'une phrase

#### **Pour importer des fichiers pré enregistrés**

Obi peut importer des fichiers wav & mp3.

Sélectionner une section dans le Volet *table des matières* ou une
phrase dans le Volet *contenu.*

*Appuyer sur Ctrl+I pour importer les fichiers audio.*

Obi crée une nouvelle phrase pour chaque fichier importé.

### 4. Vérifications et édition

Une fois placé, le contenu doit être vérifié et édité si nécessaire. Il
est possible de séparer l'audio en phrases en utilisant la fonction
**Appliquer la détection de phrases** sur l'élément sélectionné ou sur
plusieurs éléments (Menu **Phrases**). En utilisant les touches flèches
haut et bas on peut sélectionner une section, et l'écouter ou l'éditer.

Au niveau de la phrase et de son contenu audio il est possible de
**Regrouper les phrases**(**Ctrl+M**) ou **Séparer les
phrases**(**Ctrl+q**). Au niveau des sections il est également possible
de **Regrouper**(**Ctrl+Shift+M**) ou **Séparer**(**Ctrl+Shift+Q**)

### 5. Marquage des pages

Pour indiquer les numéros des pages il faut sélectionner la phrase
contenant l'enregistrement du numéro de page (par exemple Page 56) et
appuyer sur **Ctrl+Shift+G**ou aller au menu **Phrase / Assigner un rôle
/ Page**, entrer les valeurs adéquates tel que le numéro de page et
cliquer sur OK. Il est possible d'assigner d'autres rôles tels que la
couverture, le titre ou les notes.

###  

### 6. Métadonnées

Le Volet métadonnées est caché par défaut au démarrage du logiciel. Pour
l'afficher, appuyer sur **Ctrl+Alt+M** ou dans le menu **Affichage /
Afficher le volet des métadonnées.**

Plus un fichier contient de métadonnées précises, plus il sera facile de
l'identifier pour l'utilisateur comme pour le producteur.

Les métadonnées sont basées sur deux normes internationales : DublinCore
(dc) qui permet d'identifier des objets numériques, notamment les pages
internet, et Digital Talking Book (dtb) spécifiques au format Daisy.

**Les métadonnées obligatoires sont :**
-   **dc:Creator :** le nom du créateur du fichier.
-   **dc:Identifier** : un identifiant unique généré par le logiciel qui
    peut-être remplacé par un isbn si le livre numérique s'est vu
    assigné un isbn différent de celui de l'ouvrage original.
-   **dc:Language** : la langue de la publication au format RFC4646. Par
    exemple :

    -   fr : français (moderne)

    -   fr-FR : français de France, hors langues régionales normalisées
        (oc, ca, br, etc.)

    -   fr-BE : français de Belgique.

Liste complète à l'adresse :
(<https://docs.sdl.com/LiveContent/content/en-US/SDL_MediaManager_241/concept_A9F20DF9433C46FF8FED8FA11A29FAA0>).
-   **Dc:Title** : le titre de l'ouvrage.

**Plusieurs autres ? sont importantes comme :**
-   **dc:Source** permet de renseigner l'isbn de l'édition à partir de
    laquelle le livre a été adapté.
-   **dc:Right** permet de préciser le cadre légal d'exploitation du
    fichier. Par exemple la loi exception handicap.
-   **Dtb:Narrator** identifie le donneur de voix.
-   **Dc:Publisher** identifie le producteur du livre audio.

###  

### 7. Exportation

Il est nécessaire de supprimer toutes les pistes audio non utilisées
pour ne pas alourdir le poids du fichier. Utiliser la fonction
**Ctrl+L** (**Outils / Nettoyer l'audio non référencé**).

Pour finaliser le livre, Utiliser **Ctrl+E** ou le menu **Outils /
Exporter vers Daisy/Epub**, choisir le format dans la boîte de dialogue.

Il est possible d'exporter plusieurs formats à la fois, de sélectionner
le dossier où sera placé le livre exporté ainsi que de compresser les
fichiers audio au format mp3 pour alléger le poids du fichier.

### 8. Validation

Il est nécessaire de valider le livre audio avant de le distribuer pour
s'assurer qu'il est conforme aux standards et pourra être lu sans erreur
par les appareils des lecteurs.

Pour cela il faut utiliser le menu **Outils** puis sélectionner le
**Validateur** correspondant au format du fichier exporté. Si un
problème est rencontré, un rapport d'erreur sera affiché. Il faut
corriger les erreurs référencées et exporter le fichier à nouveau.

Autres fonctionnalités
----------------------

Obi dispose également de nombreuses commandes qui permettent de
sélectionner des portions d'audio et d'éditer très précisément les
enregistrements.

Il est possible d'utiliser les fonctions de copier, couper, coller ou
supprimer au niveau des sections, des phrases, d'extraits audios ou
textes.

Plusieurs fonctions permettent aussi de faciliter l'édition des fichiers
audios.

La grande variété de raccourcis clavier permet aussi de diminuer le
temps de production.

 
-

Point sur les formats
---------------------

Un format Daisy se présente sous la forme d'un dossier incluant
plusieurs fichiers et sous dossiers. Le format epub se présente sous la
forme d'un seul fichier avec l'extension .epub, il peut être renommé en
.zip et décompresser pour retrouver un ensemble de fichiers et de sous
dossiers.

### Le format daisy 2

C'est aujourd'hui le plus petit dénominateur commun permettant de
toucher le plus grand nombre de publics. Il a deux limitations
principales :
-   il ne laisse pas le choix au lecteur de consulter ou non une note de
    bas de page, celle-ci lui est toujours lue à son emplacement dans le
    texte,
-   il ne permet pas la lecture en dehors des applications et matériels
    dédiés.

### Le format Daisy 3

Il assure une meilleure gestion des notes de bas de page, laissant à
l'utilisateur le choix de consulter ou non la note au moment de la
lecture. Sa limitation principale est de ne pas être prise en charge par
une majorité des outils de lecture Daisy.

### Le format epub3

Il a la capacité d'assurer la meilleure expérience de lecture et permet
une lecture en dehors des applications et matériels dédiés, offrant
ainsi une grande liberté au lecteur.Il peut répondre notamment aux
besoins des usagers utilisant un smartphone pour lire, mais aussi à une
grande variété de publics empêchés de lire, notamment les usagers
malvoyants ou dyslexiques. Il n'est pas pris en charge à ce jour par
les appareils dédiés aux non voyants, mais peut être exploité via un
appareil connecté à un ordiphone ou tout équipement similaire.
