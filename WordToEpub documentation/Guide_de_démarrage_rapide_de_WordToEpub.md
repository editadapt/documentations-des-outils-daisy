![DAISY Consortium logo](medias/Daisy_logo.png)
<img align="right" src="medias/WordToEpub_logo.png" alt="Logo for EPUB in a circle reflecting
the same blue color as the DAISY Consortium
logo">


**Guide de démarrage rapide de WordToEpub**
===================================

(version du convertisseur WordToEpub concernée : 0.5.2)

Introduction
------------

L'objectif de ce convertisseur est de pouvoir utiliser Microsoft Word
pour produire des documents au format epub3 accessible. Il inclut les
fonctionnalités les plus avancées du format EPUB comme les descriptions
d'images et des options de navigation avancées. Les fichiers epub
peuvent être lus par toute application de lecture dédié sur téléphone,
tablette ou ordinateur indifféremment du système d'exploitation et
permet de personnaliser l'affichage des couleurs ou la taille du texte.
Certaines applications de lecture permettent également d'utiliser des
dispositifs de braille éphémère ou audio et supportent l'ajout de
commentaires ou de marques pages.

Qui peut utiliser WordToEpub ?
--------------------------

Si vous êtes **maître de conférences ou enseignant** et que vos élèves veulent lire vos documents sur support numérique. Des millions de livres scolaires et d'articles existent au format EPUB. Vous pouvez créer vos propres fichiers dans un format pensé pour être lus sur les outils de lecture les plus variés : ordinateur, tablette, téléphone ou matériel spécifique. Ils peuvent ainsi personnaliser leur expérience de lecture et l'adapter à la diversité de leurs besoins.

Si vous êtes un **auteur, journaliste ou rédacteur de rapport** et que vous créez vos propres fichiers, vous pouvez utiliser le format adapté à travers le monde par les éditeurs. Vous pouvez contrôler le rendu final et finaliser un livre électronique prêt à être distribué sur votre site ou transféré vers de multiples solutions de distribution en confiance.

Si vous êtes une **équipe produisant des livres adaptés** pour des personnes empêchées de lire, vos professionnels et bénévoles pourront utiliser leurs compétences habituelles en éditant le document avec Mircosoft Word et utiliser les fonctions avancées du convertisseur pour générer des livre numériques accessibles qui pourront être lus sur une grande variété de dispositifs incluant les lecteurs Daisy et les lecteurs Braille.

Contexte du convertisseur Word to EPUB
-----------------------------------
Cet outil est développé dans le cadre d’un projet bénévole accueillit par le Consortium DAISY. Il propose une méthode simple mais puissante pour créer des fichiers EPUB à partir de documents Microsoft Word. 

Requirements
------------

Pour utiliser WordToEPUB vous aurez besoin de :

-   Windows 7, 8 ou 10 (les versions précédentes peuvent fonctionner mais n'ont pas été testées)

-   Word 2010 ou plus récent (les versions précédentes peuvent fonctionner)

Dans le futur nous souhaitons proposer des versions pour OSX, une application mobile et un service en ligne.

Télécharger
-----------

Vous trouverez les informations à jour concernant le convertisseur ainsi que le téléchargement de la derniére version à l'adresse <https://daisy.org/wordtoepub>

Installer, mettre à jour et désinstaller l'outil
------------------------------------------------

Démarrer le programme d'installation (**setupWordToEPUB.exe**) et suivez
les indications. Il vous sera proposé d'ajouter un icône au bureau et
d'installer un bouton dans le ruban Word.

Lors du démarrage du programme, il vous sera proposé de mettre à jour si
une nouvelle version est disponible. Vous pouvez désactiver la recherche
automatique des mises à jour dans le menu **Settings** qui vous
permettra aussi de vérifier manuellement les mises à jour.

Pour désinstaller le programme, utilisez la fonction **Ajouter ou
supprimer un programme** du système d'exploitation Windows.

Créer un document accessible avec Microsoft Word
-----------------------------------------------

La qualité et l'accessibilité du fichier epub que vous allez créer
dépendra du document Word original. Assurez vous de :

-   utiliser les styles de titre et de citation proposés par le
    logiciel, ainsi que les listes à puces ou listes numérotées.

-   placer les images "inline", les images flottantes seront placées
    aléatoirement dans le contenu).

-   fournir un texte alternatif pour les images contextuelles non
    décoratives. Vous pouvez vous aider du guide de description d'images
    disponible à l'adresse
    [[http://diagramcenter.org]](http://diagramcenter.org/)

-   insérer des tableaux word, plutôt que des images de tableaux.

-   insérer les notes de bas de page ou de fin en utilisant la fonction
    dédiée du logiciel

-   si vous incluez des liens dans votre document, ils seront reproduits
    dans le fichier epub

-   inclure les informations comme le titre et l'auteur dans les
    propriétés du document.

Le document **Créer des documents Word accessibles**donne des informations détaillées.

Plusieurs sources d'information sur internet permettent d'apprendre à
construire un document word accessible. Microsoft propose un guide en
français à l'adresse suivante :
[[https://support.office.com/fr-fr/article/rendre-vos-documents-word-accessibles-aux-personnes-atteintes-de-handicaps-d9bf3683-87ac-47ea-b91a-78dcacb3c66d?ui=fr-FR&rs=fr-FR&ad=FR]](https://support.office.com/fr-fr/article/rendre-vos-documents-word-accessibles-aux-personnes-atteintes-de-handicaps-d9bf3683-87ac-47ea-b91a-78dcacb3c66d?ui=fr-FR&rs=fr-FR&ad=FR)



Utilisez le vérificateur d'accessibilité
-----------------------------

Microsoft word dispose d'une fonction de vérification de l'accessibilité
qui vous aidera à identifier les points faibles de votre document. Cette
fonction peut-être placée à différents endroits selon la version de
word, dans les versions les plus récente elle se trouve dans l'onglet
**Révision** du ruban word, sélectionnez **Vérification de
l'accessibilité.**

![Capture d’écran du ruban word montrant le bouton Vérifier l’accessibilité](medias/wtepub_fr_verif_access.png)

Dans de plus anciennes versions, sélectionnez le menu **Fichier** puis
**Informations** et cochez la case **Vérifier l'accessibilité** dans le
menu déroulant.

The latest versions of Word provide the best accessibility features and
the checker is easier to use.

For more details and step by step guidance see these pages:

<https://support.office.com/en-us/article/make-your-word-documents-accessible-to-people-with-disabilities-d9bf3683-87ac-47ea-b91a-78dcacb3c66d>

Utilisez le convertisseur DAISY WordToEPUB
-------------------------------

Le convertisseur peut-être utilisé directement dans le logiciel
microsoft word ou en tant qu'application indépendante.

**Dans Word :** le document que vous voulez convertir doit être ouvert.
Sélectionnez l'onglet **Accueil** du ruban word puis **Save as EPUB**.
À l’étape suivante confirmez le nom de fichier et le dossier où vous souhaitez l’enregistrer, validez sur **OK**. Vous venez de fabriquer un EPUB !

![Capture d’écran du ruban word montrant le bouton WordToEPUB](media/image4.png)

**Comme application indépendante :** Démarrez l'application
**WordToEPUB** depuis le menu ou le raccourci sur le bureau puis lorsque
vous y êtes invités, sélectionnez le document que vous voulez convertir.

Vous pouvez également utiliser le menu contextuel sur le document que vous
souhaitez convertir et sélectionnez **Convert with WordToEPUB**.

![S Capture d’écrandu menu contextuel montrant le choix Convert
with WordToEPUB](media/image5.png){width="2.9291043307086615in"
height="2.2557458442694664in"}

Confirmez le nom du fichier converti ainsi que le dossier où vous
souhaitez l'enregistrer puis validez sur **OK**. Vous venez de fabriquer un EPUB !

De nombreuses fonctions supplémentaires sont disponible via le bouton **Avancé** et vous permettront de changer l’image de couverture, la langue, le sens du texte et de la pagination, les métadonnées et plus encore. Reportez vous au document [Guide d’utilisation avancée de WordToEpub.md](Guide_d_utilisation_avancée_de_WordToEpub.md)

Lisez votre fichier epub  sur le dispositif de votre choix
----------------------------------------------


Vous pouvez désormais lire votre fichier epub sur n'importe quel
dispositif. Plusieurs logiciels de lecture sont disponibles, une liste
évaluant l'accessibilité des solutions de lecture est disponible à
l'adresse
[[https://inclusivepublishing.org/rs-accessibility/]](https://inclusivepublishing.org/rs-accessibility/)

Fonctions avancées

Ce guide a pour objectif une prise en main rapide du convertisseur. Pour apprendre a exploiter les fonctions avancées référez vous au document qui l’accompagne **Guide d’utilisation avancée de WordToEpub**.

Au sujet de WordToEPUB
----------------

Sélectionnez le bouton **À propos** pour en savoir plus sur le convertisseur. Les notes de version y sont disponibles et vous pourrez participer en donnant vos retours d’expérience. Vous pouvez aussi nous faire parvenir un email à l’adresse <wordtoepub@daisy.org>