![DAISY Consortium logo](medias/Daisy_logo.png)
<img align="right" src="medias/Word_logo.png" alt="Logo Microsoft Word">

[[Site Internet : daisy.org]](http://www.daisy.org/)

Création de document accessibles aux formats DOCX et PDF
======================================
(Version du convertisseur WordToEpub concernée : 0.1.3)


Ce document est destiné à être une ressource d'auto-apprentissage pour
ceux qui veulent créer des documents Word et PDF accessibles. En
utilisant les informations fournies ici et les vidéos d'accompagnement
ainsi que les ressources Web, les personnes devraient être en mesure de
créer des documents en format Word et PDF faciles à utiliser pour tout
le monde, y compris les personnes handicapées.

À savoir
----------------


Ce matériel de formation utilise les ressources créées par Microsoft sur
l'utilisation de l'outil Vérificateur d'accessibilité Office. Les
didacticiels et vidéos sources se trouvent sur le [[site Web de
Microsoft.]](https://support.office.com/en-us/article/video-check-the-accessibility-of-your-document-9d660cba-1fcd-45ad-a9d1-c4f4b5eb5b7d?ui=en-US&rs=en-US&ad=US)

Pourquoi penser à l'accessibilité ?
-------------------------------


Les personnes handicapées peuvent avoir des difficultés à lire des
documents que vous créez. Des obstacles cachés peuvent parfois empêcher
ou restreindre l'accès aux utilisateurs handicapés, en particulier les
personnes atteintes de cécité, de faible vision, d'absence ou
d'altération des couleurs, de troubles de la lecture ou encore d'une
mobilité réduite.

Ces personnes utilisent les documents numériques de différentes façons.
Certains veulent qu'ils soient lus à haute voix en utilisant une
synthèse vocale, ou affichés un dispositif braille éphémère, tandis que
d'autres préfèrent agrandir la taille des lettres et changer la couleur
du texte ou de l'arrière-plan pour l'adapter à leur vision. Certains
de vos lecteurs peuvent utiliser le clavier pour naviguer dans vos
documents tandis que d'autres peuvent utiliser le toucher, les
commandes vocales, une souris modifiée, les technologies Head Stylus ou
Eye Tracking.

Vous devez garder à l'esprit que le contenu que vous créez sera lu de
différentes façons. Si vous ne prenez pas cela en compte en créant le
contenu, des millions de personnes trouveront qu'il est difficile ou
impossible d'accéder à vos informations.

L'Organisation mondiale de la santé estime qu'environ 15% de la
population mondiale vit avec une certaine forme de
handicap]](http://www.who.int/disabilities/world_report/2011/report/en/),
donc un nombre important de personnes est susceptible d'être privé du
droit à l'information si le contenu n'est pas dans un format qu'ils
peuvent adapter en fonction de leurs besoins de lecture. En plus d'être
une nécessité, l'accessibilité numérique est également une exigence
réglementaire de nombreuses conventions et lois internationales et
nationales.

Normes d'accessibilité
-------------------------------

Répondre aux nécessités des personnes ayant des besoins divers peut
paraître compliqué pour les créateurs de contenu. Heureusement, vous
n'avez pas à penser aux besoins de chaque handicap, ni tester vos
produits sur toutes les technologies d'assistance. Il existe des normes
et des bonnes pratiques reconnues pour créer un contenu numérique
accessible. Certaines des normes les plus adoptées sont :
[[WCAG]](https://www.w3.org/WAI/intro/wcag), [[Section
508]](https://www.section508.gov/) et
[[PDF/UA]](https://en.wikipedia.org/wiki/PDF/UA).
L'adhésion à l'une d'entre elles assure que les documents seront
utilisables par tout le monde, y compris les personnes handicapées, sans
obstacle important.

Les lignes directrices sur l'accessibilité et les meilleures pratiques
pour la création de documents numériques visent à atteindre les
objectifs suivants :

1.  **Créer un document structuré et navigable.
    **Il doit être possible pour tous les lecteurs de facilement
    identifier et de passer à n'importe quel endroit du document, comme
    un chapitre ou une sous-section. Les tableaux, les listes, les notes
    etc. doivent être créés en utilisant les fonctionnalités des
    logiciels plutôt qu'avec des méthodes personnalisées.

2.  **Décrire textuellement les éléments graphiques.**
    Comme les images, des diagrammes de flux et des cartes de sorte que
    les lecteurs malvoyants ne ratent pas des aspects importants pour
    comprendre le document.

3.  **Fournir un format adaptable qui est marqué sémantiquement**
    Les lecteurs doivent être en mesure d'adapter la présentation
    visuelle du document en fonction de leurs besoins en lecture. La
    signification des différents éléments de texte doit être véhiculée
    non seulement par la présentation visuelle, par exemple la couleur,
    l'alignement mais aussi par l'utilisation de styles intégrés
    appropriés.

Microsoft propose un **Vérificateur d'accessibilité** dans ses
applications Office. S'assurer que vos documents passent les tests de
ce vérificateur d'accessibilité est généralement suffisant pour
garantir que les personnes ayant des handicaps différents n'auront pas
de difficulté majeure à utiliser le contenu. Dans cette formation, le
vérificateur d'accessibilité de Microsoft Office est utilisé comme
référence. Notez que l'accessibilité pour chaque utilisateur ne peut
pas être garantie, cependant, les documents qui passent le vérificateur
d'accessibilité et les tests manuels (décrits plus loin) seront
certainement plus accessibles et faciles à utiliser.

Lorsque le vérificateur d'accessibilité a confirmé que le document Word
est accessible, il peut être enregistré comme document PDF. Ce fichier
PDF hérite de tout le balisage lié à l'accessibilité fait dans le
document Word et sera utilisable, par les personnes handicapées.

Les fichiers PDF peuvent être testés en fonction des spécifications
WCAG, de la section 508 ou PDF/UA. Le test et la rectification des
erreurs d'accessibilité nécessitent des connaissances et des outils
supplémentaires qui ne font pas partie de cette formation.

pour créer des documents DOCX et PDF accessibles
-------------------------------


Il est préférable de penser à l'accessibilité dès le début d'un
projet. Il est souhaitable que les auteurs du contenu soient
sensibilisés aux exigences en matière d'accessibilité afin que les
concepteurs ou les techniciens impliqués dans l'édition n'aient pas à
consacrer davantage de temps à l'accessibilité.

Le processus de création de fichiers Word et PDF accessibles recommandé
dans cette formation, est résumé ci-dessous.

**Étape 1 :** préparer un document structuré avec des descriptions
d'images sous Microsoft Word.

**Étape 2 :** utiliser le vérificateur d'accessibilité et corriger les
erreurs si nécessaire

**Étape 3 :** utiliser au moins une technologie d'assistance comme NVDA
(recommandée) pour tester l'expérience de lecture et identifier les
barrières d'accessibilité restantes

**Étape 4 :** le document Word accessible est prêt. L'enregistrer au
format PDF si nécessaire.
<!--Image supprimée, il faudrait refaire le schéma en Français-->
<!-- [Schéma montrant les 4 étapes listées ci-dessus pour créer un document word accessible.](medias/WA_image2.png) -->

### Etape 1 : Création de documents Word structurés

#### En-têtes

Des titres clairs et bien formatés peuvent aider à s'assurer que vos
documents Word respectent les normes d'accessibilité internationales.
Beaucoup de gens utilisent des lecteurs d'écran pour créer une liste de
rubriques afin de survoler le document et trouver le contenu qui les
intéresse. Mais ce type de navigation ne fonctionne que si l'auteur du
document utilise des styles de titre. Les lecteurs d'écran et les
outils de synthèse vocale sont programmés pour les reconnaître.

*Cette étape est expliquée dans la vidéo -* [*[Améliorer
l'accessibilité des
en-têtes]*](https://support.office.com/en-us/article/video-improve-accessibility-with-heading-styles-68f1eeff-6113-410f-8313-b5d382cc3be1?ui=en-US&rs=en-US&ad=US)
[*(en anglais).*](http://diagramcenter.org/table-of-contents-2.html)

Pour ajouter un style de titre au texte sous Word, sélectionnez le
texte, choisissez l'onglet **ACCUEIL** dans le ruban, puis dans la boîte
de **STYLES**, choisissez le style de titre souhaité.

Lorsque vous enregistrez ou convertissez votre document dans un autre
format, tel que PDF ou HTML, les styles de titres sont conservés pour
pouvoir être utilisés et créer un sommaire ou une liste des titres.

Utilisez toujours les styles de titre dans un ordre logique et ne sautez
pas des étapes. Par exemple, le **titre 1** sera toujours suivi du
**titre 2** et le titre 2 sera suivi du **titre 3** ou d'un autre titre
2. Si cela n'est pas fait, le document ne passera pas les tests du
vérificateur d'accessibilité. En outre, les utilisateurs de lecteurs
d'écran peuvent penser qu'ils ont manqué un titre ou qu'ils sont
trompés par l'ordre.

Laisser le VOLET DE NAVIGATION ouvert est très utile quand on applique
les styles de titre. Cliquez sur l'ONGLET **AFFICHAGE**, puis
sélectionnez **VOLET DE NAVIGATION** pour ouvrir la fenêtre qui affiche
la liste de toutes les rubriques du document.

![Capture d'écran du volet
Navigation.](medias/WA_image3.png)

#### Texte alternatif ou descriptions d'images

Les utilisateurs aveugles et malvoyants ne peuvent pas voir et
comprendre le contenu non textuel, par exemple des images, des
graphiques, des cartes dans le document. Les lecteurs d'écran et les
appareils braille peuvent cependant lire la description de texte
également appelé texte alternatif que vous fournissez à la place du
contenu graphique.

*Cette étape est expliquée dans la vidéo - [[Améliorer l'accessibilité
avec le texte
alternatif]](https://support.office.com/en-us/article/video-improve-accessibility-with-alt-text-9c57ee44-bb48-40e3-aad4-7647fc1dba51?ui=en-US&rs=en-US&ad=US)*
[*(en anglais).*](http://diagramcenter.org/table-of-contents-2.html)

Pour ajouter une description à un objet, utilisez le menu contextuel ou
faites un CLIC DROIT sur l'objet et choisissez **FORMATER L'IMAGE** ou
**PROPRIÉTÉ**. Cliquez ensuite sur **TEXTE ALTERNATIF** et dans le champ
Capture d'écran du volet Navigation., tapez ou collez le texte de la
description de l'image.

*Notez que dans certaines versions de Microsoft Word, l'option Texte
alternatif peut être placée différemment. *

Pour ajouter une description à un graphique, utilisez le menu contextuel
ou faites un CLIC DROIT sur le graphique, choisissez **PROPRIETES DU
GRAPHIQUE**. Sélectionnez ensuite l'ONGLET **TEXTE ALTERNATIF**. Dans
la zone de description, saisissez une brève description du graphique,
puis cliquez sur **OK**.

##### Directives pour l'écriture de texte alternatif :

1.  Utilisez le texte alternatif pour transmettre la fonction ou le
    contenu important de l'objet.

2.  Soyez concis, généralement vous n'avez besoin que de quelques mots
    parfois une courte phrase ou deux

3.  Souvent, l'objet est décrit dans le texte environnant. Dans ce cas,
    votre texte alternatif doit être très bref et ne doit pas être une
    répétition des informations déjà fournies dans le document.

4.  Puisque les lecteurs d'écran disent généralement quel type de
    contenu l'objet est, vous n'avez pas besoin d'expressions comme
    \"image de\", \"tableau de\" ou \"lié à\". Vous devez cependant
    informer s'il s'agit d'une photographie, d'un dessin ou d'un schéma.

5.  Les images décoratives ne doivent pas être décrites. Assurez-vous
    que le texte alternatif est vide.

*Un ensemble complet de [[Lignes directrices de description d'images
est disponibles sur le site web du centre DIAGRAMME (en
anglais).]](http://diagramcenter.org/table-of-contents-2.html)
*

Si vous pensez que votre lecteur a besoin de plus d'informations, vous
pouvez écrire la description juste en dessous de l'objet. Pour les
objets complexes comme les graphiques, on écrit souvent la description
sous l'objet. Il est également courant de commencer le texte de la
description avec les mots \"Description de l'image :\"

#### Hyperliens

Si vous avez des hyperliens dans le document, la modification de leur
texte d'affichage en langage ordinaire peut les rendre beaucoup plus
facile à comprendre pour les utilisateurs qui comptent sur les
programmes de lecture d'écran.

*Cette étape est expliquée dans la vidéo - [[Créer des liens
accessibles]](https://support.office.com/en-us/article/video-create-accessible-links-in-word-28305cc8-3be2-417c-a313-dc22082d1ee0?ui=en-US&rs=en-US&ad=US)
[(en anglais).](http://diagramcenter.org/table-of-contents-2.html)*

Utilisez le menu contextuel ou faites un CLIC DROIT sur le lien
hypertexte de la page. Choisissez ensuite **MODIFIER L'HYPERLIEN**.
Dans la zone **TEXTE A AFFICHER**, saisissez une description et cliquez
sur **OK**.

Lorsque vous ajoutez du texte d'affichage, évitez les expressions comme
*Cliquer ici* ou *En savoir plus*. Les utilisateurs de logiciels de
lecture d'écran s'appuient sur la liste des hyperliens pour parcourir
le document. Si le texte d'affichage de tous ces liens est la même
expression générique, ces utilisateurs ne seront pas en mesure de
différencier le but des hyperliens.

#### Tableaux

Gardez à l'esprit ce qui suit en créant des tableaux dans vos documents
Word.

-   Utilisez la fonctionnalité Microsoft Word par défaut pour créer des
    > tableaux. Ne dessinez pas un tableau en utilisant des lignes.

-   Essayez de faire un tableau aussi simple que possible. Les lecteurs
    > d'écran et autres technologies d'assistance ne marchent pas bien
    > avec des tableaux complexes. Évitez d'utiliser des cellules
    > fusionnées, des cellules fractionnées et des tableaux imbriqués.

-   Définissez la largeur du tableau et la colonne en pourcentages de
    > manière à ce qu'il s'ajuste aux différentes tailles d'écran et
    > de page.

*Cette étape est expliquée dans la vidéo - [[Créer des tableaux
accessibles]](https://support.office.com/en-us/article/video-create-accessible-tables-in-word-cb464015-59dc-46a0-ac01-6217c62210e5?ui=en-US&rs=en-US&ad=US)
[(en anglais).](http://diagramcenter.org/table-of-contents-2.html)*

Voici comment vérifier l'accessibilité de vos tableaux. Tout d'abord,
essayez de naviguer sur tout votre tableau en utilisant uniquement la
touche **Tabulation**. Si vous pouvez naviguer facilement dans le
tableau, cellule par cellule et ligne par ligne, un lecteur d'écran ne
devrait pas rencontrer de problème. Dans les tableaux en langue latine,
la touche Tabulation doit permettre de se déplacer de gauche à droite,
en commençant dans la cellule supérieure gauche et en se terminant en
bas à droite.

Ensuite, considérez l'utilisation d'une ligne d'en-tête désignée pour
votre tableau. Les lignes d'en-tête désignées facilitent l'accès à un
lecteur d'écran pour naviguer sur votre tableau, et certains lecteurs
d'écran lisent le nom de la ligne ou de la colonne avant de lire les
données.

Pour désigner une ligne comme en-tête, sélectionnez-la, utilisez le menu
contextuel ou faites un CLIC DROIT et sélectionnez **PROPRIETES DU
TABLEAU**. Sélectionnez l'ONGLET **LIGNE**. Et cochez **RÉPÉTER COMME
LIGNE D'EN-TÊTE EN HAUT DE CHAQUE PAGE**. Assurez-vous que la **LIGNE
DE RUPTURE ENTRE LES PAGES** est décochée.

Pour fournir le texte alternatif au tableau, dans la boîte de dialogue
**PROPRIÉTÉS DU TABLEAU**, basculez vers l'ONGLET **TEXTE ALTERNATIF**
et fournissez la description du texte.

#### Listes numérotées et listes à puces

Utilisez la fonction puces et numérotation pour créer des listes dans le
document Word. Les numéros de liste ou les puces ne doivent pas être
saisies manuellement. Lorsque les listes sont créées à l'aide des
styles automatiques, les utilisateurs de technologies d'assistance sont
informés du début et de la fin des listes et aussi du nombre
d'éléments. Cela les aide à mieux comprendre le contenu.

Pour convertir une liste où les nombres par exemple a), b)\... ont été
saisis manuellement, sélectionnez le contenu entier de la liste,
utilisez le menu contextuel ou faites un CLIC DROIT, puis dans
**NUMÉROTATION** choisir un style approprié.

![Screenshot of numbering style
options](medias/WA_image4.png)

#### Nom et propriétés de fichier

Donner à vos documents des noms de fichiers et des propriétés de
document significatifs permet à tout le monde de les trouver plus
facilement. Ces étapes sont particulièrement importantes pour respecter
les nouvelles directives en matière d'accessibilité, comme
l'actualisation de la section 508 des États-Unis, la nouvelle directive
de l'UE sur l'accessibilité, et bien d'autres dans le monde. Un bon
nom de fichier fournit des indices sur le contenu du document.

*Cette étape est expliquée dans la vidéo - [[Créer des noms de fichiers
accessibles]](https://support.office.com/en-us/article/video-create-accessible-file-names-4e73d73a-aedc-47af-88e4-8f2375a69fad?ui=en-US&rs=en-US&ad=US)
[(en anglais).](http://diagramcenter.org/table-of-contents-2.html)*

Pour renommer un document dans l'Explorateur de fichiers, utilisez le
menu contextuel ou faites un CLIC DROIT sur le fichier et choisissez
**RENOMMER**. Saisissez le nouveau nom et appuyez sur **ENTRÉE**.

Lorsque le document est ouvert sous Word, vous pouvez ajouter un TITRE
et un nom d'AUTEUR aux propriétés du document, ce qui permet aux autres
de facilement trouver le fichier. L'ajout de ces propriétés fait
également partie des directives de capacité d'accès 508 des États-Unis.

Pour modifier les propriétés du document, cliquez sur **FICHIER**. Le
TITRE, l'AUTEUR et d'autres champs s'affichent sur le côté droit de la
fenêtre.

#### Utilisation de la couleur

La couleur de texte ne doit pas être utilisée seule pour transmettre des
informations dans un document. Les personnes ayant des déficiences
visuelles comme les malvoyants et les daltoniens risquent de rater ces
informations.

Dans la mesure du possible, utilisez toujours les styles.
Alternativement, le texte coloré peut être souligné. Si vous utilisez la
couleur dans les graphiques, complétez le codage de couleur avec la
texture, les différences dans le modèle de ligne, le texte dans les
graphiques, ou les nuances d'une couleur pour améliorer
l'accessibilité. L'impression d'un document couleur en noir et blanc
est le meilleur moyen de voir si vous avez perdu un élément
significatif.

Prenez également soin du contraste de couleurs, évitez de mettre des
couleurs très similaires les unes sur les autres. Un bon contraste entre
le texte et la couleur de fond permet à tout le monde de lire le
document en particulier ceux qui ont des déficiences visuelles.

#### Espacement, alignement et marges

Très souvent, nous appuyons sur la touche ENTRÉE plusieurs fois pour
créer l'espace blanc désiré entre les paragraphes. La touche TABULATION
est aussi couramment utilisée pour positionner le texte ou créer un
effet d'indentation. Ces lignes vierges et espaces blancs seront
mentionnés par les lecteurs d'écrans. Cette mise en forme crée également
des problèmes lors de la conversion du document vers d'autres formats.
Utilisez les fonctionnalités intégrées de Word comme les
**indentations**, **l'interlignage** et les **styles** pour obtenir la
présentation visuelle souhaitée.

Pour créer un espace supplémentaire avant ou après un paragraphe sans
appuyer sur entrée, utilisez le menu contextuel ou faites un CLIC DROIT
et allez à **PARAGRAPHE**. Sous **ESPACEMENT**, réglez les options
**Avant**, **Après** et **INTERLIGNE**.

![Screenshot of the Paragraph dialog
box](medias/WA_image5.png)

#### Zones de texte, graphiques et objets

Le logiciel de lecture d'écran peut ignorer les images flottantes, les
graphiques et autres objets ou lire leur texte alternatif dans le
mauvais ordre. Il est recommandé d'utiliser l'option \"en ligne avec
le texte\" ou \"haut et bas\" dans l'habillage de texte.

Pour modifier l'habillage des images, utilisez le menu contextuel ou
faites un CLIC DROIT dessus, puis cliquez sur **TAILLE ET POSITION**.
Basculez vers l'ONGLET **HABILLAGE DE TEXTE** et sélectionnez **EN
LIGNE AVEC LE TEXTE**.

![Text wrapping tab in Image Size and position dialog
](medias/WA_image6.png)

#### Autres points

-   Utiliser un langage simple en créant le contenu

-   Définir la langue du contenu. Cela aide la technologie d'assistance
    comme les lecteurs d'écran à choisir la voix convenable pour la
    lecture.\
    Pour définir la langue, sélectionnez le texte, cliquez sur
    **VÉRIFIER**, puis sur **LANGUE**, puis sur **DÉFINIR LANGUE.** Le
    **menu d'édition des styles de paragraphes** permet aussi de
    spécifier la langue.

-   Assurez-vous que la taille de police du document est suffisamment
    grande. La taille minimale généralement utilisée est 11 points.

-   Évitez d'utiliser des filigranes. Ils peuvent influer sur la
    lisibilité et créer un faible contraste.

-   Dans les documents plus longs, insérez la table des matières générée
    automatiquement pour faciliter la navigation

### Étape 2 : Utilisation du vérificateur
d'accessibilité

L'outil vérificateur d'accessibilité détecte les problèmes
d'accessibilité dans vos documents Word. Il est également disponible
pour tester des feuilles de calcul Excel, des e-mails Outlook et des
présentations PowerPoint. L'outil génère un rapport des problèmes
susceptibles de rendre votre contenu difficile à comprendre pour les
personnes handicapées. Le vérificateur d'accessibilité explique
également pourquoi et comment vous devez résoudre ces problèmes.

*Le vérificateur d'accessibilité est expliqué dans cette vidéo -
[[Vérifier l'accessibilité du
document]](https://support.office.com/en-us/article/video-check-the-accessibility-of-your-document-9d660cba-1fcd-45ad-a9d1-c4f4b5eb5b7d?ui=en-US&rs=en-US&ad=US)
[(en anglais).](http://diagramcenter.org/table-of-contents-2.html)*

#### Démarrer le vérificateur d'accessibilité

Si vous êtes un utilisateur Office 365, recherchez le bouton **VÉRIFIER
L'ACCESSIBILITÉ** sur l'ONGLET **VÉRIFICATION** du ruban. Cliquez
dessus pour ouvrir le vérificateur d'accessibilité.

Si vous possédez une version antérieure de Word comme celle de 2010 et
que vous ne voyez pas le bouton Vérifier l'accessibilité dans l'onglet
Vérification du ruban, procédez comme suit pour ouvrir le vérificateur
d'accessibilité.

1.  Cliquez sur FICHIER \> INFO.

2.  Sélectionnez le bouton VÉRIFIER LES PROBLÈMES.

3.  Dans le menu déroulant vérifier les problèmes, sélectionnez VÉRIFIER
    L'ACCESSIBILITÉ.

> ![Check for Issues option in Word File menu
> ](medias/WA_image7.png)

4.  Le VOLET DE TÂCHE DE CONTRÔLE D'ACCESSIBILITÉ s'affiche à côté de
    votre contenu et montre les résultats de l'inspection.

> ![Inspection Results
> group](medias/WA_image8.png){width="2.0277777777777777in"
> height="3.9368055555555554in"}

5.  Pour savoir pourquoi et comment résoudre un problème, sous
    **Résultats d'inspection**, sélectionnez un problème. Les résultats
    apparaissent sous **Informations supplémentaires**, et vous êtes
    dirigé vers le contenu inaccessible de votre fichier.

> ![Additional Information
> group](medias/WA_image9.png)

#### Comprendre les résultats de l'inspection

Une fois que le vérificateur d'accessibilité inspecte votre contenu, il
signale les résultats de l'inspection en fonction de la gravité du
problème trouvé, classé comme suit :

-   **Erreurs**. Les problèmes qui sont signalés comme des erreurs
    comprennent le contenu très difficile ou impossible à comprendre
    pour les personnes handicapées.

-   **Avertissements**. Les avertissements, le plus souvent, signifient
    que le contenu est difficile à comprendre pour les personnes
    handicapées.

-   **Conseils**. Les conseils vous font savoir que, même si les
    personnes handicapées peuvent comprendre le contenu, il peut être
    mieux organisé ou présenté pour améliorer leur expérience.

#### Erreurs du vérificateur d'accessibilité, avertissements et astuces

Les tableaux suivants dénombrent les règles du vérificateur
d'accessibilité, ce qu'elles vérifient, où apprendre comment et
pourquoi vous devez résoudre chaque problème.

##### Erreurs

Si le contenu dans le fichier est très difficile ou impossible à
utiliser pour une personne ayant un handicap, le vérificateur
d'accessibilité le classifie comme une erreur.

<table width="100%" cellpadding="10" cellspacing="0">
		<col width="57*"/>

		<col width="66*"/>

		<col width="134*"/>

		<thead>
			<tr valign="top">
				<td width="22%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Règle
					</b></span></font></font>
					</p>
				</td>
				<td width="26%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Le
					vérificateur d'accessibilité vérifie</b></span></font></font></p>
				</td>
				<td width="52%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Pourquoi
					réparer ça ? </b></span></font></font>
					</p>
				</td>
			</tr>
		</thead>
		<tbody>
			<tr valign="top">
				<td width="22%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Tous
					les contenus non textuels ont un texte alternatif (texte alt).</b></span></font></font></p>
				</td>
				<td width="26%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Tous
					les objets ont un texte alternatif et le texte ne contient pas
					d'images ou d'extensions de fichier.</span></font></font></p>
				</td>
				<td width="52%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Les
					lecteurs d'écran lisent à haute voix le texte alternatif pour
					décrire des images et d'autres contenus non textuels que les
					utilisateurs ne peuvent pas voir. En fonction du texte
					alternatif du contenu non textuel, les utilisateurs doivent
					comprendre le but et la signification. </span></font></font>
					</p>
				</td>
			</tr>
			<tr valign="top">
				<td width="22%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Les
					tableaux spécifient les informations <br/>
d'en-tête de
					colonne. </b></span></font></font>
					</p>
				</td>
				<td width="26%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Les
					tableaux et/ou les blocs de cellules ont une zone d'en-tête
					sélectionnée ou une ligne d'en-tête indiquée.</span></font></font></p>
				</td>
				<td width="52%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Les
					utilisateurs s'appuient sur les en-têtes de tableaux pour
					comprendre le contenu qui est lu par la suite par le lecteur
					d'écran. En outre, la technologie d'assistance utilise souvent
					la ligne d'en-tête du tableau pour aider l'utilisateur à
					connaître l'emplacement actuel du curseur et fournir des
					informations qui permettent à l'utilisateur de naviguer. </span></font></font>
					</p>
				</td>
			</tr>
			<tr valign="top">
				<td width="22%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Les
					documents utilisent des styles de rubriques </b></span></font></font>
					</p>
				</td>
				<td width="26%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Le
					contenu est organisé avec des rubriques et/ou une table des
					matières (TDM).</span></font></font></p>
				</td>
				<td width="52%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Les
					rubriques et les TDM fournissent un contexte structurel aux
					utilisateurs et permettent la navigation et une recherche plus
					facile dans le document.</span></font></font></p>
				</td>
			</tr>
		</tbody>
	</table>

##### Avertissements

Si le contenu dans la plupart (mais pas nécessairement tous) des cas est
difficile à comprendre pour les personnes handicapées, le vérificateur
d'accessibilité donne un avertissement.

<table width="100%" cellpadding="10" cellspacing="0">
		<col width="52*"/>

		<col width="84*"/>

		<col width="120*"/>

		<thead>
			<tr valign="top">
				<td width="20%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Règle
					</b></span></font></font>
					</p>
				</td>
				<td width="33%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Le
					vérificateur d'accessibilité vérifie</b></span></font></font></p>
				</td>
				<td width="47%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Pourquoi
					réparer ça ?</b></span></font></font></p>
				</td>
			</tr>
		</thead>
		<tbody>
			<tr valign="top">
				<td width="20%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Le
					texte du lien hypertexte est sensé. </b></span></font></font>
					</p>
				</td>
				<td width="33%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Le
					texte de lien est sensé comme information autonome, en
					fournissant des informations exactes sur la cible de
					destination. </span></font></font>
					</p>
				</td>
				<td width="47%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">En
					fonction du texte, les utilisateurs décident s'il faut cliquer
					sur un hyperlien. Le texte doit fournir des informations claires
					sur la destination du lien.</span></font></font></p>
				</td>
			</tr>
			<tr valign="top">
				<td width="20%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Le
					tableau a une structure simple.</b></span></font></font></p>
				</td>
				<td width="33%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Les
					tableaux sont des rectangles simples sans cellules fractionnées,
					cellules fusionnées ou imbrication.</span></font></font></p>
				</td>
				<td width="47%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Les
					utilisateurs naviguent dans les tableaux via les raccourcis
					clavier et la technologie d'assistance, qui s'appuient sur des
					structures de tableaux simples.</span></font></font></p>
				</td>
			</tr>
			<tr valign="top">
				<td width="20%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Les
					tableaux n'utilisent pas de cellules vides pour la mise en
					forme. </b></span></font></font>
					</p>
				</td>
				<td width="33%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Il
					n'y a pas de lignes ou de colonnes entièrement vides dans le
					tableau.</span></font></font></p>
				</td>
				<td width="47%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Les
					cellules de tableau vides peuvent tromper un utilisateur en lui
					faisant penser qu'il n'y a plus de contenu dans le tableau.</span></font></font></p>
				</td>
			</tr>
			<tr valign="top">
				<td width="20%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Évitez
					l'utilisation de caractères vierges répétés. </b></span></font></font>
					</p>
				</td>
				<td width="33%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Il
					n'y a pas de série d'espaces vierges, d'onglets ou de retours
					chariot.</span></font></font></p>
				</td>
				<td width="47%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Les
					espaces, les onglets et les paragraphes vides sont souvent lus
					comme vierges par la technologie d'assistance. Après avoir
					entendu plusieurs &quot;vides&quot;, les gens peuvent penser
					qu'ils ont atteint la fin de l'information.</span></font></font></p>
				</td>
			</tr>
		</tbody>
	</table>

##### Conseils

Quand il y a du contenu que les personnes handicapées peuvent
comprendre, mais qui pourrait être mieux organisé ou pourrait être
présenté d'une manière qui peut améliorer leur expérience, vous voyez
un conseil.
<table width="100%" cellpadding="10" cellspacing="0">
		<col width="64*"/>

		<col width="73*"/>

		<col width="119*"/>

		<thead>
			<tr valign="top">
				<td width="25%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Règle
					</b></span></font></font>
					</p>
				</td>
				<td width="29%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Le
					vérificateur d'accessibilité vérifie</b></span></font></font></p>
				</td>
				<td width="46%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Pourquoi
					réparer ça ? </b></span></font></font>
					</p>
				</td>
			</tr>
		</thead>
		<tbody>
			<tr valign="top">
				<td width="25%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Les
					tableaux de présentation sont structurés pour faciliter la
					navigation. </span></font></font>
					</p>
				</td>
				<td width="29%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">L'ordre
					de présentation est logique pour la langue, et l'ordre de
					tabulation n'est pas circulaire.</span></font></font></p>
				</td>
				<td width="46%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Les
					utilisateurs comptent sur la présentation du tableau pour
					naviguer dans le contenu. Il doit être ordonné logiquement
					pour que les utilisateurs comprennent et naviguent dans le
					contenu.</span></font></font></p>
				</td>
			</tr>
			<tr valign="top">
				<td width="25%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Aucun
					filigrane d'image n'est utilisé. </span></font></font>
					</p>
				</td>
				<td width="29%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Il
					n'y a pas de filigrane.</span></font></font></p>
				</td>
				<td width="46%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Les
					filigranes peuvent être mal compris et considérés comme
					faisant partie du contenu principal de la page et peuvent être
					à l'origine d'une confusion.</span></font></font></p>
				</td>
			</tr>
			<tr valign="top">
				<td width="25%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Toutes
					les rubriques sont dans le bon ordre. </span></font></font>
					</p>
				</td>
				<td width="29%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Toutes
					les rubriques suivent un ordre logique.</span></font></font></p>
				</td>
				<td width="46%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
					<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Les
					rubriques séquentielles avec les niveaux appropriés aident les
					utilisateurs à naviguer, à rechercher et à comprendre
					l'organisation du document.</span></font></font></p>
				</td>
			</tr>
		</tbody>
	</table>

Après avoir corrigé tous les problèmes signalés par le vérificateur
d'accessibilité, vous recevrez le message *« aucun problème
d'accessibilité trouvé. Les personnes handicapées ne devraient pas
avoir de difficulté pour lire ce document »*. Votre objectif doit être
d'avoir ce statut avant de distribuer tout document Word.

![Accessibility Checker no issues found message
](medias/WA_image10.png)

### Étape 3 : Utilisation de NVDA pour des tests
d'accessibilité manuels

NVDA est un logiciel libre de lecture d'écran pour Windows. Il peut
être utilisé pour vérifier l'expérience de lecture que les personnes
ayant des déficiences visuelles auront. NVDA peut également aider à
identifier les problèmes qui ne peuvent pas être détectés par le
vérificateur d'accessibilité, comme l'adéquation du texte alternatif
de l'image. Le vérificateur d'accessibilité peut uniquement tester si
les images ont ou non des descriptions de texte, pas si la description
du texte est appropriée ou non à l'image.

À l'aide des touches NVDA ci-dessous, vous pouvez consulter le document
pour lire l'ordre, la navigabilité et les descriptions des images.

1.  Téléchargez et installez NVDA sur le site NV-ACCESS :
    [[nvaccess.org/download]](https://www.nvaccess.org/download/)

2.  Démarrez NVDA en cliquant sur son ICÔNE SUR LE BUREAU ou en
    utilisant la combinaison des touches CTRL + ALT + N.

3.  Sélectionnez \"UTILISER VERROUILLAGE MAJUSCULE comme TOUCHE DE
    MODIFICATION NVDA\" dans la boîte de DIALOGUE DE BIENVENUE.
    Dorénavant, appuyez sur VERROUILLAGE MAJUSCULE au lieu de la TOUCHE
    NVDA mentionnée ci-dessous.

4.  Appuyez sur la TOUCHE NVDA + CTRL + S pour sélectionner un
    SYNTHÉTISEUR VOCAL. Vous aimerez peut-être les voix de MICROSOFT
    SAPI.

5.  Utilisez maintenant les touches répertoriées ci-dessous pour
    vérifier les différents aspects de l'accessibilité du document.

<table width="100%" cellpadding="10" cellspacing="0">
			<col width="52*"/>

			<col width="73*"/>

			<col width="131*"/>

			<thead>
				<tr valign="top">
					<td width="20%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p style="margin-top: 0.21cm">
						<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Fonction</b></span></font></font></p>
					</td>
					<td width="29%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p style="margin-top: 0.21cm">
						<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Pression
						de touche </b></span></font></font>
						</p>
					</td>
					<td width="51%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p style="margin-top: 0.21cm">
						<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR"><b>Ce
						qu'il faut tester </b></span></font></font>
						</p>
					</td>
				</tr>
			</thead>
			<tbody>
				<tr valign="top">
					<td width="20%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p style="margin-top: 0.21cm">
						<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Rubriques/structure
						</span></font></font>
						</p>
					</td>
					<td width="29%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p style="margin-top: 0.21cm; margin-bottom: 0.21cm">
						<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Appuyez
						sur la TOUCHE NVDA + ESPACE pour activer le MODE DE NAVIGATION </span></font></font>
						</p>
						<p style="margin-top: 0.21cm; margin-bottom: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Puis
						appuyez sur H pour passer à la rubrique suivante</span></font></font></p>
						<p style="margin-top: 0.21cm; margin-bottom: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">MAJ
						+ H pour passer au titre précédent </span></font></font>
						</p>
						<p style="margin-top: 0.21cm; margin-bottom: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">NVDA
						lira le titre et annoncera son &quot;NIVEAU&quot; </span></font></font>
						</p>
						<p style="margin-top: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Appuyez
						sur la TOUCHE NVDA + F7 pour ouvrir la LISTE DES ÉLÉMENTS et
						vérifier l'ordre et la hiérarchie des rubriques.</span></font></font></p>
					</td>
					<td width="51%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm">
						<ol><li><p style="margin-top: 0.21cm; margin-bottom: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Le
							document doit commencer par une rubrique 1. Toutes les
							sections/chapitres principaux doivent être marqués au même
							niveau. </span></font></font>
							</p>
							<li><p style="margin-top: 0.21cm; margin-bottom: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Le
							niveau de rubrique ne doit pas être ignoré, par exemple une
							rubrique 3 après la rubrique 1 est une violation des
							directives </span></font></font>
							</p>
							<li><p style="margin-top: 0.21cm; margin-bottom: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Un
							document doit avoir un nombre suffisant de rubriques. Un long
							document avec très peu de rubriques coure le risque d'échouer
							aux tests du vérificateur d'accessibilité.</span></font></font></p>
							<li><p style="margin-top: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Le
							texte du titre ne doit pas être trop long </span></font></font>
							</p>
						</ol>
					</td>
				</tr>
				<tr valign="top">
					<td width="20%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p style="margin-top: 0.21cm">
						<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Graphiques
						</span></font></font>
						</p>
					</td>
					<td width="29%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p style="margin-top: 0.21cm; margin-bottom: 0.21cm">
						<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">En
						MODE NAVIGATION : </span></font></font>
						</p>
						<p style="margin-top: 0.21cm; margin-bottom: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Appuyez
						sur G pour accéder au graphique suivant</span></font></font></p>
						<p style="margin-top: 0.21cm; margin-bottom: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">MAJ
						+ G vers le graphique précédent </span></font></font>
						</p>
						<p style="margin-top: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">NVDA
						lira le texte alternatif du graphique s'il est disponible </span></font></font>
						</p>
					</td>
					<td width="51%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm">
						<ol><li><p style="margin-top: 0.21cm; margin-bottom: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Écoutez
							la voix NVDA tout en naviguant entre les graphiques.</span></font></font></p>
							<li><p style="margin-top: 0.21cm; margin-bottom: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Vérifiez
							si la description lue par NVDA est sensée et appropriée </span></font></font>
							</p>
							<li><p style="margin-top: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Souvent
							NVDA peut lire le nom du fichier de l'image ou sa taille, etc.
							Dans ce cas, le TEXTE ALTERNATIF doit être vérifié en
							allant aux PROPRIÉTÉS DE L'IMAGE </span></font></font>
							</p>
						</ol>
					</td>
				</tr>
				<tr valign="top">
					<td width="20%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p style="margin-top: 0.21cm">
						<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Tableaux
						</span></font></font>
						</p>
					</td>
					<td width="29%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p style="margin-top: 0.21cm; margin-bottom: 0.21cm">
						<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">En
						MODE NAVIGATION :</span></font></font></p>
						<p style="margin-top: 0.21cm; margin-bottom: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Appuyez
						sur T pour le tableau suivant <br/>
MAJ + T pour le tableau
						précédent </span></font></font>
						</p>
						<p style="margin-top: 0.21cm; margin-bottom: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Appuyez
						sur la touche ONGLET pour naviguer entre les cellules d'un
						tableau </span></font></font>
						</p>
						<p lang="fr-FR" style="margin-top: 0.21cm"><br/>

						</p>
					</td>
					<td width="51%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm">
						<ol><li><p style="margin-top: 0.21cm; margin-bottom: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Seules
							les données tabulaires doivent être représentées sous la
							forme d'un tableau.</span></font></font></p>
							<li><p style="margin-top: 0.21cm; margin-bottom: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">En
							appuyant sur la touche ONGLET, vérifiez si le focus de NVDA
							bouge dans le tableau en ordre logique. En général, le
							tableau doit être lu de gauche à droite rangée par rangée.
							Les cellules fractionnées ou fusionnées peuvent être
							présentes pour la compréhension du tableau.</span></font></font></p>
							<li><p style="margin-top: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">NVDA
							lira les en-têtes de colonne et de ligne pendant que vous
							naviguez. Vérifiez si l'association d'en-têtes est
							appropriée pour les cellules lues. </span></font></font>
							</p>
						</ol>
					</td>
				</tr>
				<tr valign="top">
					<td width="20%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p style="margin-top: 0.21cm">
						<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Hyperliens
						</span></font></font>
						</p>
					</td>
					<td width="29%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p style="margin-top: 0.21cm; margin-bottom: 0.21cm">
						<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">En
						MODE NAVIGATION :</span></font></font></p>
						<p style="margin-top: 0.21cm; margin-bottom: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Appuyez
						sur K pour le lien hypertexte suivant </span></font></font>
						</p>
						<p style="margin-top: 0.21cm; margin-bottom: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">MAJ
						+ K pour le lien hypertexte précédent </span></font></font>
						</p>
						<p style="margin-top: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Appuyez
						sur la TOUCHE NVDA + F7 pour ouvrir les ÉLÉMENTS, puis
						vérifiez la liste des liens. </span></font></font>
						</p>
					</td>
					<td width="51%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
						<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Écoutez
						le NVDA et vérifiez si le texte du lien est lu correctement.
						En général, les URL doivent avoir un texte d'affichage sensé.
						</span></font></font>
						</p>
					</td>
				</tr>
				<tr valign="top">
					<td width="20%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p style="margin-top: 0.21cm">
						<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Ordre
						de lecture </span></font></font>
						</p>
					</td>
					<td width="29%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p style="margin-top: 0.21cm; margin-bottom: 0.21cm">
						<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Touche
						NVDA + FLÈCHE VERS LE BAS pour lire en continu </span></font></font>
						</p>
						<p style="margin-top: 0.21cm; margin-bottom: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">FLÈCHE
						VERS LE BAS pour lire la ligne suivante </span></font></font>
						</p>
						<p style="margin-top: 0.21cm"><font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">FLÈCHE
						VERS LE HAUT pour lire la ligne précédente </span></font></font>
						</p>
					</td>
					<td width="51%" style="background: transparent" style="border: 1px solid #000000; padding: 0cm 0.19cm"><p class="western" style="margin-top: 0.21cm">
						<font color="#000000"><font face="Times New Roman, serif"><span lang="fr-FR">Vérifiez
						si NVDA lit le document dans l'ordre logique. Cela doit être
						testé lorsque des colonnes, des encadrés, des zones de texte
						sont présents dans le document. Les directives d'accessibilité
						exigent que le document ait un ordre de lecture simple et
						logique </span></font></font>
						</p>
					</td>
				</tr>
			</tbody>
		</table>

6.  Appuyez sur la touche CTRL pour interrompre la voix (Couper NVDA
    temporairement)

7.  Appuyez sur la TOUCHE NVDA + Q pour quitter NVDA

Si des problèmes sont détectés lors des tests NVDA, ils doivent être
corrigés et le document doit être vérifié avec l'outil vérificateur
d'accessibilité à nouveau.

### Étape 4 : Enregistrer comme Word et PDF
accessible

Une fois que le vérificateur d'accessibilité signale que le document
n'a pas de problèmes et que les tests manuels avec NVDA ne signalent
pas non plus des soucis, le document Word est prêt pour la distribution.

Si une version PDF est nécessaire, il faut suivre les étapes suivantes :

1.  Cliquez sur FICHIER \> ENREGISTRER SOUS ou appuyez simplement sur
    F12

2.  Dans ENREGISTRER SOUS LE TYPE, choisissez PDF

3.  Vous pouvez modifier le nom du fichier et choisir un répertoire pour
    enregistrer le fichier et cliquer sur OK.

4.  Le fichier PDF qui en résulte est prêt pour la distribution.

## Lcture et assistance

Guides en français pour l'accessibilité des documents bureautiques 
[[https://github.com/DISIC/guides-documents\_bureautiques\_accessibles]]((https://github.com/DISIC/guides-documents\_bureautiques\_accessibles))

Plus d'informations sur l'accessibilité des documents sont disponibles
sur le site Web de Microsoft
[[www.aka.ms/accessible]](http://www.aka.ms/accessible)*.*

Toutes les directives sur la description d'images sont disponibles sur
le centre du DIAGRAMME.
[[diagramcenter.org]](http://diagramcenter.org/table-of-contents-2.html)

Abonnez-vous à la newsletter de DAISY Planet pour obtenir les dernières
informations sur l'accessibilité numérique -
[[daisy.org/contact]](http://www.daisy.org/contact) *(en
anglais)*

Des ressources sur la création de publications accessibles sont
disponibles sur le site Web de la publication inclusive --
[[inclusivepublishing.org](https://inclusivepublishing.org/)]
*(en anglais)*

Le support technique du consortium Daisy peut être contacté pour tout
ce qui concerne l'accessibilité numérique. Veuillez soumettre vos
questions en utilisant [le formulaire
Contactez-nous :](https://inclusivepublishing.org/)
[[daisy.org/contact]](http://www.daisy.org/contact)

