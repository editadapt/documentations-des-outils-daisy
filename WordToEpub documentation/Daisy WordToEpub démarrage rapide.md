# Démarrer avec le convertisseur Daisy WordToEpub

Note : une version française de l'interface est en cours d'élaboration. 

## Introduction



Cet outil est développé par le Consortium Daisy, il fonctionne sur le
système d'exploitation windows 7, 8 ou 10 et Microsoft Word à partir de
la version 2010. L'interface n'est actuellement disponible qu'en
anglais.

## Installer, mettre à jour et désinstaller l'outil

Démarrer le programme d'installation (**setupWordToEPUB.exe**) et suivez
les indications. Il vous sera proposé d'ajouter un icône au bureau et
d'installer un bouton dans le ruban Word.

Lors du démarrage du programme, il vous sera proposé de mettre à jour si
une nouvelle version est disponible. Vous pouvez désactiver la recherche
automatique des mises à jour dans le menu **Settings** qui vous
permettra aussi de vérifier manuellement les mises à jour.

Pour désinstaller le programme, utilisez la fonction **Ajouter ou
supprimer un programme** du système d'exploitation Windows.

## Créer un document accessible avec Microsoft Word

La qualité et l'accessibilité du fichier epub que vous allez créer
dépendra du document Word original. Assurez vous de :

-   utiliser les styles de titre et de citation proposés par le
    logiciel, ainsi que les listes à puces ou listes numérotées.

-   placer les images "inline", les images flottantes seront placées
    aléatoirement dans le contenu).

-   fournir un texte alternatif pour les images contextuelles non
    décoratives. Vous pouvez vous aider du guide de description d'images
    disponible à l'adresse
    [[http://diagramcenter.org]](http://diagramcenter.org/)

-   insérer des tableaux word, plutôt que des images de tableaux.

-   insérer les notes de bas de page ou de fin en utilisant la fonction
    dédiée du logiciel

-   si vous incluez des liens dans votre document, ils seront reproduits
    dans le fichier epub

-   inclure les informations comme le titre et l'auteur dans les
    propriétés du document.

Plusieurs sources d'information sur internet permettent d'apprendre à
construire un document word accessible. Microsoft propose un guide en
français à l'adresse suivante :
[[https://support.office.com/fr-fr/article/rendre-vos-documents-word-accessibles-aux-personnes-atteintes-de-handicaps-d9bf3683-87ac-47ea-b91a-78dcacb3c66d?ui=fr-FR&rs=fr-FR&ad=FR]](https://support.office.com/fr-fr/article/rendre-vos-documents-word-accessibles-aux-personnes-atteintes-de-handicaps-d9bf3683-87ac-47ea-b91a-78dcacb3c66d?ui=fr-FR&rs=fr-FR&ad=FR)

## utilisez le vérificateur d'accessibilité

Microsoft word dispose d'une fonction de vérification de l'accessibilité
qui vous aidera à identifier les points faibles de votre document. Cette
fonction peut-être placée à différents endroits selon la version de
word, dans les versions les plus récente elle se trouve dans l'onglet
**Révision** du ruban word, sélectionnez **Vérification de
l'accessibilité.**

![Screenshot of the Word ribbon showing the Check Accessibility
button](media/wtepub_fr_verif_access.png){width="5.877083333333333in"
height="1.3347222222222221in"}

Dans de plus anciennes versions, sélectionnez le menu **Fichier** puis
**Informations** et cochez la case **Vérifier l'accessibilité** dans le
menu déroulant.

## Utilisez le convertisseur DAISY WordToEPUB


Confirmez le nom du fichier converti et le dossier où vous souhaitez
l'enregistrer puis cliquez sur **OK**.

![Screenshot of the Word ribbon showing the WordToEPUB
button](media/wtepub_fr_ruban.png){width="6.113888888888889in"
height="1.4055555555555554in"}

**Comme application indépendante :** Démarrez l'application
**WordToEPUB** depuis le menu ou le raccourci sur le bureau puis lorsque
vous y êtes invités, sélectionnez le document que vous voulez convertir.
Confirmez le nom du fichier converti ainsi que le dossier où vous
souhaitez l'enregistrer puis cliquez sur **OK**.

Vous pouvez également faire un clic droit sur le document que vous
souhaitez convertir et sélectionnez **Convert with WordToEPUB**.

![Screenshot of context menu from File Explorer showing option Convert
with WordToEPUB](media/wtepub_fr_contextuel.png){width="2.9291666666666667in"
height="2.25625in"}

Pour aller plus loin, vous pouvez sélectionner le bouton Advanced qui
vous permettra de modifier la couverture du document, modifier le
langage de la publication, le sens du texte, les métadonnées et plus
encore. Ces options sont décrites au paragraphe Fonctions avancées de ce
document.

## Conversions multiples (Batch conversions)

Cette option vous permet de convertir plusieurs documents en une seule
fois. Démarrez le programme WordToEPUB et sélectionnez les différents
documents que vous souhaitez convertir. Les options par défaut seront
appliquées, vous pouvez les modifier dans les préférences du logiciel.

## Fonctions avancées (Advanced options)

Après avoir sélectionné un fichier pour le convertir, vous pouvez
cliquer sur **display Advanced options**. Une boîte de dialogue multi
page s'ouvre proposant plusieurs options décrites ci-dessous.

## Advanced / Cover

Permet de créer une image de couverture spécifique. Sélectionnez **Use
first page of document**. Vous pouvez aussi sélectionner une image ou
choisir de ne pas avoir de couverture. Pour choisir votre image,
celle-ci devra être au format JPG ou PNG, une taille de 1600px par
2400px est conseillée. Pensez à mettre à jour le texte alternatif
correspondant à la nouvelle image de couverture en remplissant la case
**Alt text** de cet écran.

La pratique habituelle consiste à inclure la couverture au début du
contenu de l'epub, mais elle peut être inclue aux métadonnées sans être
affichée à la première page.

## Advanced / Metadata

Les métadonnées fournissent des informations sur le document qui sont
utilisées par les outils de diffusion (site web ou bibliothèque en
ligne) ainsi que les applications de lecture. Le titre (**Title**) et
l'auteur (**Author**) sont récupérés du document word si ils ont étés
correctement renseignés, vous pouvez les modifier si ce n'est pas le
cas. Il est conseillé d'ajouter un résumé sur les fonctions
d'accessibilité (**accessibility** **summary**). Vous pouvez aussi
ajouter une **description** et la mention d'édition (**Publisher**).

## Advanced / EPUB options

Vous pouvez ici renseigner la langue utilisée dans le document
(**language**) pour assurer un coupure des mots correcte ainsi que
l'usage de la synthèse vocale adéquate si la voix dans ce langage
spécifique est présente.

Un sommaire est généré dans le contenu, vous pouvez le désactiver
(**inline table of contents**), sans que cela n'impacte la fonction
spécifique du logiciel de lecture. Vous pouvez aussi spécifier le titre
de ce sommaire (**title of the inline of table of contents**) et ignorer
le sommaire inclut dans le document word.

Plusieurs exemples de feuilles de style (**CSS file**)sont proposées
pour ajuster la présentation du fichier epub. Il est possible d'ajouter
votre propre feuille de style dans le dossier d'installation (**Program
Files (x86)\\DAISY\\WordToEPUB\\CSS**).

Pour les contenus qui doivent être lus de droite à gauche, comme la
langue Arabe, sélectionnez **rtl\_ready.css **

Il est possible de diviser le contenu en plusieurs fichiers xhtml afin
de faciliter le chargement, vous pouvez sélectionner cette division
selon les niveaux de titre (1, 2, etc.) avec le menu déroulant **Split
at heading level.**

## Advanced / Page numbers

La correspondance des pages avec le livre imprimé est une fonctionnalité
du format epub qui permet de faciliter les citations, référencements et
rapproche l'expérience de la lecture imprimée même si le livre numérique
sera affiché sur un nombre de page différent de l'original en fonction
des options de présentation.

Par défaut, WordToEpub inclut la correspondance aux pages du document
word. Si la numérotation est réglée pour démarrer à 100, c'est ce qui
apparaitra dans l'index des pages du livre imprimé généré par le fichier
epub. La **source** des numérotations de page est notée dans les
métadonnées du fichier epub et peut être modifiée dans cette boite de
dialogue. Si l'ouvrage se réfère à une édition imprimée spécifique,
c'est une information importante à noter. Si la pagination de l'ouvrage
original ne correspond pas à celle du document word, vous pouvez la
renseigner en utilisant le le **niveau de titre 6** ou le style **Page
Number (DAISY).**

## Advanced / Math

Si vous utilisez la fonction de word **Insertion / Équation**, les
formules seront converties au format MathML recommandé pour les
technologies d'assistance, même si la prise en charge par ces dernière
est encore faible. Vous pouvez aussi inclure la formule mathématique
sous forme image et renseigner un texte alternatif. L'outil MathML cloud
peut vous aider :
[[https://mathmlcloud.org/]](https://mathmlcloud.org/)

## Settings

Les options peuvent être configurée et sauvegardées pour chaque
utilisateur en utilisant ce menu pour ne pas avoir à les modifier à
chaque conversion. Ce menu permet aussi de régler les message que le
logiciel affiche durant la conversion, de vérifier les mises à jour,
d'installer ou désinstaller le ruban Word. Une interface en Français est
attendue dans une futur version.

## About WordToEPUB

Ce menu permet de connaître la version du convertisseur que vous
utilisez.

## A word about Pandoc

WordToEpub utilise Pandoc, un convertisseur de document libre sous
licence GNU disponible pour toutes les plateformes. L'utilitaire en
ligne de commande réguliérement mis à jour peut être téléchargé à
l'adresse [[http://pandoc.org](http://pandoc.org/).]

## Lisez votre fichier epub !

Vous pouvez désormais lire votre fichier epub sur n'importe quel
dispositif. Plusieurs logiciels de lecture sont disponibles, une liste
évaluant l'accessibilité des solutions de lecture est disponible à
l'adresse
[[https://inclusivepublishing.org/rs-accessibility/]](https://inclusivepublishing.org/rs-accessibility/)
