![DAISY Consortium logo](../medias/Daisy_logo.png)
<img align="right" src="../medias/WordToEpub_logo.png" alt="Logo for EPUB in a circle reflecting
the same blue color as the DAISY Consortium
logo">


**Getting started with WordToEPUB**
===================================
(WordToEpub version : 0.5.2)

Introduction
------------

Using your word processor, you can convert documents to the latest EPUB
3 format. EPUB is a wonderful format for reading publications on
laptops, tablets, and smartphones, and it includes features such as rich
navigation and great accessibility. The EPUB files created with this
tool can be used in a wide variety of reading apps on any platform, with
the ability to personalize visual features such as colors, font, text
size, and layout. Many reading apps have other useful features such as
read aloud, the ability to add comments and bookmarks and support for
electronic braille.

Who should use WordToEPUB?
--------------------------

Are you a **lecturer or professor** with students who want to read their
materials in digital form? There are millions of digital textbooks and
journal articles in EPUB format. Now you can provide your documents to
students in a format designed for reading on the devices your students
use (whether computer, tablet or smartphone), with an experience that
can be personalized by each individual to meet their diverse needs.

Are you an **author or report writer** creating your own work? Now you
can use the same publishing format as adopted by the publishing industry
wordwide. You can check how your work will appear as you go, and make an
eBook for distributing from your own blog or upload to multiple retail
platforms with confidence.

Are you a **team making accessible books** for people with print
disabilities? With Word ToEPUB your volunteers and staff can use
familiar skills to edit materials in Word, and then using the advanced
features of WordToEPUB generate valid and accessible digital books that
can be read with a wide variety of apps and devices, including audio
players and braille displays.

Background to the Word to EPUB tool
-----------------------------------

This tool is developed as a volunteer project under the auspices of the
DAISY Consortium. It provides a simple but powerful method to create
EPUB files from Microsoft Word documents. You can also produce EPUB from
Google Docs, Apple Pages and Libre Office, but you should find you get
the best results with the combination of Microsoft Word and WordToEPUB
from the DAISY Consortium.

Requirements
------------

To use WordToEPUB will will need:

-   Windows 7, 8 or 10 (earlier versions may work but are untested)

-   Word 2010 or newer (earlier versions may also work)

In the future we may provide versions for macOS, mobile and in the
cloud.

Downloading
-----------

You can read about WordToEPUB and download the current release at:

<https://daisy.org/wordtoepub>

Installation, updating and uninstalling the tool
------------------------------------------------

Start the setup program and follow the simple steps. You will have the
option to add a desktop icon and to install a button on the ribbon as a
Word Add-in.

When you run the program, if there is a newer version available it will
prompt you to update. You can turn this automatic checking off in
Settings. Also, in Settings, you can manually check for an update.

To uninstall the program, use the Windows "Add or remove programs"
feature. You can remove the Word Add-in this way also.

Create an accessible document in Microsoft Word
-----------------------------------------------

You can use regular Word documents to make EPUBs. If you make a great
Word document, you will get a great EPUB. Make sure that you:

-   use the built-in styles for headings, this is necessary as the
    absolutely bare minimum

-   use the proper Word feature for bulleted and numbered lists

-   for a blockquote, use the built-in styles for **Block Text**

-   ensure that images are inline (floating images will be included but
    the placement in the text will be unpredictable)

-   provide alt text for non-decorative images that are not described in
    the surrounding text. For guidance on image description visit
    [http://diagramcenter.org](http://diagramcenter.org/))

-   insert proper Word tables, not images of tables

-   if you have references to footnotes and endnotes, use the proper
    Word feature (i.e. **References** / **Insert Endnote**)

-   if you include web addressed in the document, make them links
    (**Insert** / **Link**) and the user will be able to activate them
    in the EPUB

-   include information such as **title** and **author** in the document
    properties

In addition to our document **Creating accessible Word documents**
there's lots of information freely available on the web to help you make
accessible Word documents.

Use the accessibility checker
-----------------------------

Make sure you've done your best to make an accessible document with the
built-in accessibility checker in Word. This will help you identify
issues that aren't readily apparent. The way you get to this varies
between versions. On recent editions of Word, move to the **Review** tab
on the ribbon. Then select **Check Accessibility**.

![Screenshot of the Word ribbon showing the Check Accessibility
button](../medias/image3.png)

On older versions, select **File \> Info** and select the Check for
Issues button. Then select **Check Accessibility** from the drop-down
menu.

The latest versions of Word provide the best accessibility features and
the checker is easier to use.

For more details and step by step guidance see these pages:

<https://support.office.com/en-us/article/make-your-word-documents-accessible-to-people-with-disabilities-d9bf3683-87ac-47ea-b91a-78dcacb3c66d>

Using the DAISY WordToEPUB tool
-------------------------------

The tool can be used from within Word itself, or as a standalone
application.

**From within Word:** the document you want to convert should be open.
From the Home ribbon, select "WordToEPUB". In the next step, confirm the
filename and folder for the EPUB and select OK. Congratulations, you
made an EPUB!

![Screenshot of the Word ribbon showing the WordToEPUB
button](../medias/image4.png)

**As a standalone program:** Start the WordToEPUB program from the menu
or desktop shortcut, then select the document you want to convert.

Alternatively, use File Explorer to select the Word document you wish to
convert, then press right-click (or shift-F10) to bring up the context
menu and select "Convert with WordToEPUB".

![Screenshot of context menu from File Explorer showing option Convert
with WordToEPUB](../medias/image5.png)

Confirm the filename and folder for the EPUB and select OK.
Congratulations, you made an EPUB!

For additional features, select the \"Advanced\" button. Now you can
change the cover image, language, page/text direction, metadata and
more. See [More advanced features](#_More_advanced_features)

Read the EPUB on your choice of reading system
----------------------------------------------

Now you have your EPUB, you can open it in an app that supports your
reading preferences. Whilst you made the EPUB on Windows, you can read
it on Windows, macOS, Android, iOS, in a browser or on a special reading
device.

The DAISY Consortium coordinates accessibility testing of EPUB reading
systems by volunteers and accessibility experts. For more information
about reading systems, including reviews and detailed test results, you
can visit <https://inclusivepublishing.org/rs-accessibility/>

Advanced features
-----------------

This guide should get you up and running with WordToEPUB, but the tool
can do so much more. Learn about this in the companion document
**Advanced Guide to WordToEPUB**.

About WordToEPUB
----------------

Select the About button to learn more about the WordToEPUB tool. The
release notes for the installed version can be read from here, and you
can also submit feedback. We'd love to hear from you! If you prefer you
can send us email at <wordtoepub@daisy.org>
