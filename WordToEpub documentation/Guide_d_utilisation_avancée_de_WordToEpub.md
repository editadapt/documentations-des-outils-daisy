![DAISY Consortium logo](medias/Daisy_logo.png)
<img align="right" src="medias/WordToEpub_logo.png" alt="Logo for EPUB in a circle reflecting
the same blue color as the DAISY Consortium
logo">


**Guide d'utilisation avancée de WordToEpub**
================================
(version du convertisseur WordToEpub concernée : 0.5.2)

Introduction
------------

Vous pouvez commencer à utiliser WordToEpub facilement en utilisant le document **Guide de démarrage rapide**. Il décrit l'installation, la mise à jour et la désinstallation ainsi que le fonctions basique
pour créer un fichier EPUB à partir d'un document word.

Ce guide donne des information sur les fonctions avancées pour ceux qui souhaitent mieux contrôler les EPUBs qu'ils créent 
et veulent utiliser des fonctionnalités plus sophistiquées.

Créer un document accessible avec Microsoft Word
-----------------------------------------------

Les EPUBs que vous réez avec ce logiciel sont convertis depuis des documents word. Vous devez être attentifs à la construction de votre document.
Des informations précises pour créer un document Word accessible sont disponibles  dans le document **Créer des documents Word accessibles**

Considérations supplémentaires sur l'édition des documents Word pour une conversion avec WordToEpub
-------------------------------------------------------

### Inclure les formules mathématiques avec MathML
Les expressions mathématiques inclues dans le format Word Mathématiques (OOML) sont convertis et placées dans le fichier EPUB au format MathML.
De plus en plus de systèmes de lecture supportent MathML, mais vous pouvez aussi choisir d'inclure les Mathématiques en tant qu'images si vous savez que vos solutions de lecture ne supportent pas MathML. Vous devrez alors ajouter un texte alternatif (voir paragraphe suivant).

Il y a plusieurs possibilités pour ajouter des expressions mathématiques à votre document : 

Utilisez **Insérer / Équation** et l'éditeur d'équations de Word pour écrire une expression qui sera enregistrée au format OOML. 
Cela fonctionne avec la majorité des équations.

Si vous connaissez le langage **Latex**, vous pouvez l'utiliser pour écrire vos expressions mathématiques. 
Utilisez  **Insérer / Équation / Insérer une nouvelle équation**. Dans le panneau **Conversion** du ruban word assurez vous que Latex est sélectionné.
Écrivez vos équations au format Latex, elles seront converties et enregistrées au format OOML.

Il existe différents outils pour créer des expressions au format MathML. Vous pouvez alors les coller dans votre document en tant que texte brut, Word les convertira
immédiatement au format OOML (Utilisez **ctrl v** pour coller, puis **ctrl** et **t**)

Si vous utilisez Wiris MathType pour créer vos expressions, elles peuvent aussi être converties au format OOML. Configurez MathType via  **MathType** /
**Préférences** / **Préférences de copier / coller** / **MathML** and /
**MathMl 3.0**.

![Capture d'écran de la boite de dialogue MathMl Préférences de copier / coller](media/image3.png){width="2.955224190726159in"
height="2.2563538932633422in"}

Utilisez ensuite l'éditeur d'expressions MathType pour ouvrir l'expression que vous souhaitez convertir.

![Capture d'écran d'une expression mathématique MathType](media/image4.png){width="2.8281255468066493in"
height="1.9055818022747157in"}

Sélectionnez et coupez l'expression (**ctrl a** puis **ctrl x**), revenez à votre document et collez-la avec **ctrl v**. 
MathType ajoutera l'expression au format OOML.


### Expressions mathématiques en tant qu'images

Si les appareils de lecture ciblés ne supportent pas Math ML, vous pouvez choisir d'inclure les équations mathématiques sous forme d'images. 

Pour utiliser cette technique l'outil MathML cloud disponible à l'adresse <https://mathmlcloud.org/> peut-être utile, il génère des descriptions utiles, mais seulement en langue anglaise.

Cette solution n'est pas idéale en termes d'accessibilité, mais elle permet de s'assurer du bon affichage sur tout dispositif. Nous espérons développer plus de fonctionnalités autour de la conversion des mathématiques au fur et à mesure que les bonnes pratique et les solutions de lecture s'améliorent.

### Métadonnées

WordToEpub offre l'opportunité d'enrichir les métadonnées lors de la conversion. En cas de conversion de plusieurs fichiers à la fois, il est nécessaire renseigner les métadonnées pour chaque fichier. Les métadonnées présentes dans le document Microsoft Word sont extraites et ajoutées lors de la conversion.

Pour ajouter des métadonnées au fichier .docx, utilisez **Fichier** / **Informations** / **Propriétés** / **Propriétés avancées**
Les champs **Titre** et **Auteur** correspondent aux informations dc:title et dc:creator. Dans le champ **Personnaliser** vous pouvez ajouter : 

-   Subtitle (sous titre)

-   Publisher (éditeur)

-   Rights (droits)

-   ISBN

-   Description 

Descriptions d'images
------------------

Les images non décoratives doivent être décrite dans le fichier docx en utilisant le menu contextuel (clic droit) puis **Format d'image** / **texte alternatif**

Pour les images décoratives ou décrites dans le texte, la case **Marquer comme décorative** doit être cochée.

Conversion unique ou par lot
----------------------------

Le **Guide de démarrage rapide** explique comment convertir un fichier word à la fois en utilisant le ruban word, l'application WordToEpub ou via le menu contextuel de l'explorateur de fichiers.

Pour convertir de nombreux documents il est possible de faire une conversion par lot. Démarrez WordToEpub et sélectionnez les documents dans la boite de dialogue **Sélectionnez le ou les fichiers à convertir**. Vous pouvez sélectionner des fichiers adjacents en utilisant la touche **Shift** ou bien des fichiers dispersés en utilisant la touche **Ctrl**. Cliquez ensuite sur **Ok** puis sélectionnez le dossier de destination où seront enregistrés les fichiers epub.

Lors de la conversion WordToEpub utilisera les options sélectionnées dans le panneau **Préférences**. ainsi que les métadonnées renseignées dans le fichier .docx.


Fonctions avancées
-----------------

Après avoir sélectionné un fichier pour le convertir, le bouton **Avancé** donne accès à des options supplémentaires. La boite de dialogue multiple qui s'ouvre propose les options suivantes. 


### Avancé / Métadonnées

Les métadonnées du fichier EPUB apportent des informations au sujet du document pour les applications de lecture ainsi que pour les circuits de distribution. 

Les champs minimums à remplir sont **Titre** et **Auteur**. La boite de dialogue vous permet d'éditer les métadonnées récupérées du fichier word ou bien d'en ajouter. 

Comme décrit au paragraphe **Métadonnées** de ce document, les valeurs peuvent être déterminées via Word pour les champs 

-  Title (titre)

-   Author (auteur)

-   Subtitle (sous titre)

-   Publisher (éditeur)

-   Rights (droits)

-   ISBN

-   Description

La **Date** sera celle du jour de la conversion, et **Source** permet d'identifier le document duquel est reproduite la numérotation, par exemple l'ISBN de l'ouvrage imprimé.

Un résumé d'accessibilité est recommandé car utile pour informer les lecteurs des fonctionnalités ou des risques. Un exemple est proposé dans le convertisseur, il peut-être modifié ou enrichi en fonction des caractéristiques de votre fichier. 

### Avancé / Image de couverture

L'image de couverture peut-être incluse comme première page du fichier EPUB ou bien seulement être présente dans les métadonnées. Il est également possible de ne pas ajouter d'image de couverture. 

Le convertisseur peut créer une image de couverture en photographiant la première page du document via la case à cocher **Utiliser la première page du document**.

Une image de couverture par défaut est fournie par le convertisseur, mais il est recommandé de sélectionner une image correspondante au document. Les format JPG et PNG sont acceptés, une taille de 1600 par 2400 pixels assure un bon rendu. Assurez vous de mettre à jour le **Texte alternatif de l'image de couverture**.


### Avancé / options EPUB
Dans le futur le convertisseur devrait détecter automatiquement le langage du document. Pour l'heure vous pouvez le renseigner ici. Cela assure une lecture correct pour une synthèse vocale, mais permet aussi au logiciel de lecture d'ajuster les règles typographies appliquées lors de l'affichage du document. 

Cette boite de dialogue permet aussi d'indiquer le sens de lecture du texte, par exemple de gauche à droite pour un document en langue française, et de droite à gauche pour un document en langue arabe.

Les applications de lecture permettent d'afficher le sommaire du document sans avoir besoin de l'inclure au texte affiché. Le convertisseur ne génère donc pas de sommaire affiché. Il est possible de changer ce comportement ici. Dans ce cas il est possible de personnaliser le titre de ce sommaire.

Le document Word peut avoir déjà inclut cette table des matières, le convertisseur l'ignorera sauf si vous le modifiez ici. 

Différentes feuilles de style sont proposées avec le convertisseur, elles permettent de contrôler et d'ajuster l'apparence du texte en fonction des solutions de lecture utilisé. Il est possible d'ajouter une feuille de style personnalisée dans le dossier **Program Files (x86)\\DAISY\\WordToEPUB\\CSS**. Une feuille de style spécifique assure la bonne gestion des contenus lus de droite à gauche. 

Le fichier word sera découpé en plusieurs fichiers XHTML pour être inclus dans le fichier EPUB. Généralement c’est le style **titre 1** qui détermine le découpage de ces fichiers. Il est possible de modifier ce comportement ici.


### Avancé / Pagination

La navigation par pages est une fonctionnalité puissante du format EPUB, le numéro de page peut ainsi être le même quelque soit la taille de l'écran ou des caractères. La référence à un numéro de page vous amènera ainsi toujours au même endroit quelque soit votre modalité de lecture. Le document imprimé est généralement utilisé comme référence.

Par défaut le convertisseur utilise les numéros de page du document word, et ce même si ils ne sont pas affichés dans le document. Si le document est paramétré pour commencer à la page 100, il en sera ainsi dans le fichier EPUB. La source du document peut-être modifiée dans le champ **Source** du panneau **Métadonnées**, pour correspondre par exemple à un ouvrage imprimé.

Des équipes spécialisées dans l'adaptation de document disposent de méthodes alternatives pour marquer les numéros de page de l'ouvrage source sans dépendre de la pagination du document word. 
WordToEpub permet d'utiliser plusieurs de ces méthodes. 

Le numéro de page peut être inclut dans le contenu du texte et marqué avec le style **Titre 6**, le style **Page Number (DAISY)**, ou placé en début de ligne précédé de la mention **PRINT PAGE**, en sélectionnant le type de numérotation dans ce panneau, le convertisseur les utilisera comme référence de page. 


Préférences
-----------
Les options décrites ci-dessus peuvent être paramétrées et enregistrées pour chaque utilisateur via les panneaux **Options de conversion par défaut** et **Couverture par défaut**.

Le fonctionnement du convertisseur peut-être ajusté via le panneau **Options de l'interface utilisateur**. La boîte de dialogue de démarrage de l'application peut-être désactivée ici. Il est aussi possible d'afficher le menu **Avancé** par défaut, et les messages lors de la conversions peuvent être affichés. Ce panneau permet aussi de sélectionner la langue de l'interface utilisateur.

Le panneau **Ruban Word** permet d'ajouter ou de supprimer un bouton de conversion rapide à la barre d'outil de Microsoft Word.

Le panneau **mises à jour** permet de vérifier les nouvelles versions du logiciels, et choisir de les installer automatiquement ou non.


Dépannage
---------------

### Le fichier EPUB est lu dans une autre langue. 

Le langage n'est pas détecté automatiquement, il est nécessaire de le renseigner via le menu **Avancé** / **Options EPUB** ou **Préférences** / **Options de conversion par défaut**. 

### Toutes les images n'apparaissent pas dans le fichier EPUB
Les images incluses dans le document word au format WMF ne sont pas supportées par le format EPUB. Le menu **Avancé** / **options EPUB** / **Convertir toutes les images au format PNG** permet de résoudre le problème.

### Échec de la conversion via Pandoc (error code 251)

C’est un message relatif au manque de mémoire de travail de l'ordinateur. Cette erreur se produit seulement sur des ordinateurs 32 bits. Tenter la conversion sur un ordinateur 64 bits devrait résoudre le problème.

### L'installation du ruban Word échoue.

Le bouton du ruban word permet de convertir le document depuis Microsoft Word. Si l'installation échoue, il reste possible de convertir le document via le menu contextuel de l'explorateur de fichier ou bien en démarrant l’application WordToEpub.


Un mot au sujet de Pandoc
-------------------
Un des composants de WordToEpub ets Pandoc, un fantastique outil libre de conversion sous licence GNU General Public License.

L'outil en ligne de commande est régulièrement mis à jour et disponible à l'adresse 
<http://pandoc.org>.

D'autres librairies opensource sont incluses dans WordToEpub.

Au sujet de WordToEPUB
----------------

Ce bouton affiche les notes de versions et permet de nous communiquer votre expérience d'utilisation ainsi que les erreurs ou difficultés rencontrées. Nous sommes impatients de vous lire !